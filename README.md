# weisflem

## Description

A Rust implementation of partition and refinement and Weisfeler-Leman algorithm for graph isomorphism in particular.
Other partition refinement algorithms are included: LexBFS, modular decomposition, computation of a permutation with low stabbing number.

## Installation

Install rust via rustup, and then:
```
cargo build --release
```

## Usage

```
Usage : ./target/release/weisflem [command] [...]

Apply various partition refinement techniques based on the possible
following [command]s:

wl1 [fname]
Reads edges from file [fname] (or stdin if [fname] is [-])
formatted as one per line as [u v] where [u] and [v] are two integers.
Computes WL1 color classes on the resulting (undirected) graph.

wl1iso [fname1] [fname2] 
Reads edges from file [fname1] (or stdin if [fname1] is [-])
formatted as one per line as [u v] where [u] and [v] are two integers.
Similarly read [fname2].
Run WL1 isomorphism test on both resulting (undirected) graphs.

lexbfs [fname] [src]
Reads edges from [fname] similarly as above, and performs a lexicographic BFS
from source vertex [src] in the resulting (undirected) graph.

bfs [fname] [src]
Same as lexbfs with a classical BFS.

low-crossing [fname]
Reads edges from [fname] similarly as above, and output a path v1,...,vn such
that closed neighborhoods of the resulting (undirected) graph cross few edges.
More precisely, if the graph has VC-dimension d, the overall number of crossings
is O(n^(2-1/d)) with high probability (inspired from [Duraj, Konieczny, Potepa, 2023]). 

low-crossing-dir [fname]
Same as low-crossing with a directed graph and open out-neighborhoods.

wl1er [n] [d]
Construct an Erdos-Renyi graph G_n,d/n with [n] vertices and probability
[p=d/n] for edge presence and compute color classes according to WL1, both
on the graph and on its 2-core. 

speed [n] [beta]
Construct a random power-law graph with [n] vertices and law exponent [beta],
and times various basic graph procedures on it (BFS, DFS, WL1).

core-ordering [fname]
Reads edges from [fname] similarly as above, and output a core elemination ordering.

factor-perm [fname]
Reads edges from [fname] similarly as above, and output a factorizing permutation
for strong modules, i.e. an ordering of the nodes of the input graph such that
any strong module (a module that do not overlap any other module) is a factor.

modular-decomposition [fname]
Reads edges from [fname] similarly as above, and output the tree of the modular 
decomposition, using parenthesis to delimit sub-trees. Children are sorted by 
increasing ID of the descendant with smallest ID.
```

## Authors
Laurent Viennot.

## Acknowledgment
With the help of discussions with Louann Coste, Pierluigi Crescenzi, Michel Habib, Marc Lelarge.

## License
Gnu LGPL

## Project status
Beginning.