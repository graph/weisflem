/// Dynamic graph


use crate::graph::*;

pub struct Dygraph {
    adj: Vec<Vec<Node>>,
    src: Vec<Node>, // sources: nodes with at least one out-neighbor
}

impl AGraph for Dygraph {

    fn n(&self) -> usize {
        self.adj.len()
    }

    fn m(&self) -> usize {
        self.adj.iter().map(|a| a.len()).sum()
    }

    fn deg(&self, v: Node) -> usize {
        self.adj[v as usize].len()
    }

    fn neighbors(&self, v: Node) -> &[Node] {
        &self.adj[v as usize]
    }

}

use std::cmp::max;

impl Dygraph {

    pub fn new(n: usize) -> Self {
        Dygraph { adj: Vec::with_capacity(n), src: Vec::with_capacity(n) }
    }

    pub fn sources(&self) -> &[Node] {
        &self.src
    }

    pub fn clear(&mut self) {
        for &u in &self.src {
            self.adj[u as usize].truncate(0);
        }
        self.src.clear()
    }

    pub fn from_edges(edg: &[Edge], symmetrize: bool) -> Self {
        let mut g = Self::new(0);
        for e in edg {
            g.add_arc(e.u, e.v);
            if symmetrize { g.add_arc(e.v, e.u) }
        }
        g
    }

    pub fn add_arc(&mut self, u: Node, v: Node) {
        let m = max(u, v) as usize;
        if m >= self.adj.len() { 
            self.adj.resize_with(m+1, || Vec::with_capacity(0));             
        }
        if self.adj[u as usize].len() == 0 { self.src.push(u) }
        self.adj[u as usize].push(v);
    }

    pub fn sort(&mut self) {
        for &u in &self.src {
            self.adj[u as usize].sort_unstable();
        }
    }

    pub fn clone(&self) -> Self {
        Dygraph { adj: self.adj.clone(), src: self.src.clone() }
    }

    pub fn simple(&mut self) {
        self.sort();
        for &u in &self.src {
            let u = u as usize;
            let mut i: usize = 0;
            let mut j: usize = 0;
            while j < self.adj[u].len() {
                if i < j { self.adj[u][i] = self.adj[u][j]; }
                i += 1;
                j += 1;
                while j < self.adj[u].len() && self.adj[u][j-1] == self.adj[u][j] { j += 1; }
            }
            self.adj[u].truncate(i);
        }
    }

    pub fn reverse<G: AGraph>(g: &G) -> Self {
        let mut rev = Dygraph::new(g.n());
        for u in 0..g.n() as Node {
            for &v in g.neighbors(u) {
                rev.add_arc(v, u);
            }
        }
        rev
    }

    pub fn from_agraph<G: AGraph>(g: &G) -> Self {
        let mut d = Dygraph::new(g.n());
        for u in 0..g.n() as Node {
            for &v in g.neighbors(u) {
                d.add_arc(u, v);
            }
        }
        d
    }


    pub fn crossing_number<G: AGraph>(g: &G, perm: &[Node]) -> usize {
        let mut rev = Self::reverse(g);
        rev.simple();
        let mut sum = 0;
        assert_eq!(perm.len(), rev.n());
        for i in 1..rev.n() {
            sum += rev.adj_diff(perm[i-1], perm[i]);
        }
        sum
    }

    /// Asserts sorted adjacencies (call self.sort()).
    /// Returns the number of elements in the symmetric difference of adj[u] and adj[v]:
    fn adj_diff(&self, u: Node, v: Node) -> usize {
        let mut sum : usize = 0;
        let u = u as usize;
        let v = v as usize;
        let mut i: usize = 0;
        let mut j: usize = 0;
        while i < self.adj[u].len() && j < self.adj[v].len() {
            if self.adj[u][i] < self.adj[v][j] {
                sum += 1;
                i += 1;
            } else if self.adj[u][i] > self.adj[v][j] {
                sum += 1;
                j += 1;
            } else {
                i += 1;
                j += 1;
            }
        }
        sum += self.adj[u].len() - i;
        sum += self.adj[v].len() - j;
        sum
    }

    /// Asserts sorted adjacencies (call self.sort()).
    /// Calls [f] on all nodes of the symmetric difference of adj[u] and adj[v]:
    pub fn adj_diff_iter<F: FnMut(Node) -> ()>(&self, u: Node, v: Node, mut f: F) {
        let u = u as usize;
        let v = v as usize;
        let mut i: usize = 0;
        let mut j: usize = 0;
        while i < self.adj[u].len() && j < self.adj[v].len() {
            if self.adj[u][i] < self.adj[v][j] {
                f(self.adj[u][i]);
                i += 1;
            } else if self.adj[u][i] > self.adj[v][j] {
                f(self.adj[v][j]);
                j += 1;
            } else {
                i += 1;
                j += 1;
            }
        }
        while i < self.adj[u].len() {
            f(self.adj[u][i]);
            i += 1;
        }
        while j < self.adj[v].len() {
            f(self.adj[v][j]);
            j += 1;
        }
    }

}


use std::fmt::Display;

impl Display for Dygraph {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for u in 0..self.n() as Node {
            write!(f, "{u} :")?;
            for &v in self.neighbors(u) {
                write!(f, " {v}")?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}
#[cfg(test)]
pub mod tests {

    use super::Dygraph;
    use crate::{graph::{AGraph, Node}, Edge};

    #[test]
    fn check_diff() {
        let mut g = Dygraph::new(2);
        g.add_arc(0,1);
        g.add_arc(0,3);
        g.add_arc(0,4);
        g.add_arc(1,2);
        g.add_arc(1,4);
        assert_eq!(g.adj_diff(0,1), 3);
        let mut d: usize = 0;
        g.adj_diff_iter(0, 1, |_| d+=1);
        assert_eq!(g.adj_diff(0,1), d);
    }

    #[test]
    fn check_simple() {
        let mut g = Dygraph::new(2);
        g.add_arc(0,1);
        g.add_arc(0,2);
        g.add_arc(0,1);
        g.add_arc(1,0);
        g.add_arc(1,0);
        g.add_arc(1,1);
        g.add_arc(1,0);
        g.add_arc(2,1);
        g.simple();
        println!("{}", g);
        assert_eq!(g.m(), 5);
        let perm: [u32; 3] = [0, 1, 2];
        assert_eq!(Dygraph::crossing_number(&g, &perm), 4);

        let n = 10;
        let mut g = Dygraph::new(n);
        // path of length n:
        for v in 0..n as Node {
            g.add_arc(v, v);
            if v > 0 { g.add_arc(v, v-1) }
            if v+1 < n as Node { g.add_arc(v, v+1) }
        }
        let mut perm = Vec::with_capacity(n);
        for v in 0..n { perm.push(v as Node) }
        assert_eq!(Dygraph::crossing_number(&g, &perm), 2*n-4);

        // path
        let edg = [(0 as Node, 1 as Node), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9), (9, 10)];
        let edg = edg.map(|(u,v)| Edge { u, v });
        let mut g = Dygraph::from_edges(&edg, true);
        for v in 0..g.n() as Node { g.add_arc(v, v) }
        println!("{}", g);
        let perm = [0 as Node, 6, 7, 5, 1, 4, 2, 3, 8, 10, 9];
        println!("{:?}", perm);
        assert_eq!(Dygraph::crossing_number(&g, &perm), 39);

        // test/hex2.txt
        let edg = [(0 as Node, 1 as Node), (1, 2), (2, 3), (3, 4), (4, 5), (5, 0), (1, 6), (6, 7), (7, 8), (8, 9), (9, 0)];
        let edg = edg.map(|(u,v)| Edge { u, v });
        let mut g = Dygraph::from_edges(&edg, true);
        for v in 0..g.n() as Node { g.add_arc(v, v) }
        println!("{} m={}", g, g.m());
        let perm = [8 as Node, 6, 4, 7, 0, 5, 9, 2, 3, 1];
        println!("{:?}", perm);
        assert_eq!(Dygraph::crossing_number(&g, &perm), 43);

        // sources
        let edg = [(0 as Node, 1 as Node), (1, 2), (4, 2), (6, 4), (4, 2)];
        let edg = edg.map(|(u,v)| Edge { u, v });
        let mut g = Dygraph::from_edges(&edg, false);
        assert_eq!(g.src.len(), 4);
        for &s in &g.src { assert!(g.adj[s as usize].len() > 0) }
        g.simple();
        assert_eq!(g.src.len(), 4);
        for &s in &g.src { assert!(g.adj[s as usize].len() > 0) }
    }

}