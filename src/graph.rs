/// A simple graph library to manipulate a graph where nodes are ints `0..n`.
/// 
/// Example:
/// ```
/// let g = Graph::from_edges(&[Edge {u: O, v: 1}, Edge {u: 1, v: 2}], true);
/// for u in 0..g.n() as Node {
///     for &v in g.neighbors(u) {
///         println!("{u} -> {v}");
///     }
/// }
/// assert_eq!(* g.neighbors(1).first().unwrap(), 0);
/// assert_eq!(* g.neighbors(1).last().unwrap(), 2);
/// ```

pub type Node = u32; // choose whatever is faster/smaller on your machine! (could be u32, usize, i32,...)

/// an edge
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Edge {
    /// tail
    pub u: Node,
    /// head
    pub v: Node,
}
impl Edge {
    pub fn rev(&self) -> Edge { Edge {u: self.v, v: self.u} }
    pub fn max(&self) -> Node { if self.u >= self.v {self.u} else {self.v} }
}

/// Abstract graph.
pub trait AGraph {
    /// number of nodes
    fn n(&self) -> usize;
    /// number of (directed) edges (i.e. sum of degrees)
    fn m(&self) -> usize;
    /// degree
    fn deg(&self, v: Node) -> usize;
    /// neighbors
    fn neighbors(&self, v: Node) -> &[Node];

    /// edges (by increasing tail, and heads ordered according to [neighbors()])
    fn edges(&self) -> Vec<Edge> { self._edges(true, false) }
    /// edges (only edges with tail smaller than head)
    fn edges_undirected(&self) -> Vec<Edge> { self._edges(true, true) }
    /// reversed edges (by increasing head, and tails ordered according to [neighbors()])
    fn edges_reversed(&self) -> Vec<Edge> { self._edges(false, false) }
 
     // generic edges
    fn _edges(&self, fwd: bool, undirected: bool) -> Vec<Edge> {
        let m = if undirected { self.m()/2 } else { self.m() };
        let mut edg : Vec<Edge> = Vec::with_capacity(m);
        for u in 0..self.n() as Node {
            for &v in self.neighbors(u) {
                if u < v || ! undirected {
                    if fwd {
                        edg.push(Edge { u, v });
                    } else {
                        edg.push(Edge { u: v, v: u });
                    }
                }
            }
        }
        edg
    }

    /// test adjacency
    fn has_neighbor(&self, u: Node, v: Node) -> bool {
        for &w in self.neighbors(u) {
            if w == v { return true }
        }
        false
    }

    /// subgraph
    fn subgraph_edges(&self, set: &[Node]) -> Vec<Edge> {
        let not_present = Node::MAX;
        let mut index: Vec<Node> = vec![not_present; self.n()];
        for (i, &v) in set.iter().enumerate() {
            index[v as usize] = i as Node; // renumber nodes
        }
        let mut edg: Vec<Edge> = Vec::with_capacity(set.len());
        for (i, &u) in set.iter().enumerate()  {
            for &v in self.neighbors(u) {
                let j = index[v as usize] as Node;
                if j != not_present { edg.push(Edge { u: i as Node, v: j }); }
            }
        }
        edg
    }

    /// Squared graph
    fn square_edges(&self, undirected: bool) -> Vec<Edge> {
        let m = self.m(); // (under-) estimate
        let mut edg : Vec<Edge> = Vec::with_capacity(m);
        for u in 0..self.n() as Node {
            for &v in self.neighbors(u) {
                for &w in self.neighbors(v) {
                    if u < w || ! undirected  {
                        edg.push(Edge { u, v: w });
                    }
                }
            }
        }
        edg
    }
    
}

/// A graph in compressed sparse row format. 
#[derive(Debug, PartialEq, Eq)]
pub struct Graph {
    deg_sum: Vec<usize>,
    all_adj: Vec<Node>,
}

impl AGraph for Graph {

    fn n(&self) -> usize {
        self.deg_sum.len() - 1
    }

    fn m(&self) -> usize {
        * self.deg_sum.last().unwrap()
    }

    fn deg(&self, v: Node) -> usize {
        let b = self.deg_sum[v as usize];
        let e = self.deg_sum[(v+1) as usize];
        e - b
    }

    fn neighbors(&self, v: Node) -> &[Node] {
        let b = self.deg_sum[v as usize];
        let e = self.deg_sum[(v+1) as usize];
        &self.all_adj[b..e]
    }

}


impl Graph {

    /// Construct a graph from edges listed in [edg]. If [undir] is true,
    /// each edge [uv] is considered as undirected ([v] is a neighbor of [u],
    /// and [u] is also a neighbor of [v]).
    /// Order of adjacency lists is induced by the order of the input (if [undir] is false).
    pub fn from_edges(edg: &[Edge], undir: bool) -> Self {
        // Maximal node number:
        let mut n: Node = 0;
        for e in edg {
            if e.u > n { n = e.u; }
            if e.v > n { n = e.v; }
        }

        // Degrees:
        let mut deg_sum: Vec<usize> = vec![0; (n+2) as usize];
        for e in edg {
            deg_sum[e.u as usize] += 1;
            if undir { deg_sum[e.v as usize] += 1 }
        }
        // prefix sums:
        for u in 0..=n {
            deg_sum[(u+1) as usize] += deg_sum[u as usize]
        }

        // Adjacency lists:
        let mut all_adj: Vec<Node>  = vec![0; deg_sum[(n+1) as usize]];
        let mut add_adj = |u: Node, v: Node| {
            deg_sum[u as usize] -= 1;
            all_adj[deg_sum[u as usize]] = v;
        };
        for e in edg.iter().rev() {
            add_adj(e.u, e.v);
            if undir { add_adj(e.v, e.u); }
        }

        Self { deg_sum, all_adj }
    }

    /// Empty graph with [n] nodes.
    pub fn empty(n: usize) -> Self {
        Self { deg_sum: vec![0; n+2], all_adj: vec![0; 0] }
    }

    /// Reversed graph (where edges are reversed). 
    pub fn reverse(&self) -> Self {
        let edg = self.edges_reversed();
        Graph::from_edges(&edg, false)
    }

    pub fn largest_cc(&self) -> Self {
        let mut bfs = BFS::new(self.n());
        let cc = bfs.cc(self);
        let set = bfs.largest_cc(&cc);
        let mut set = set.to_vec();
        set.sort_unstable();
        //for &v in set { eprint!("{v} ")} eprintln!();
        let edg = self.subgraph_edges(&set);
        Graph::from_edges(&edg, false)
    }

    pub fn bfs_numbering(&self) -> Self {
        let mut bfs = BFS::new(self.n());
        let perm = random_permutation(self.n());
        bfs.bfs_all(self, &perm);
        assert_eq!(bfs.fifo.len(), self.n());
        let perm = inverse_permutation(&bfs.fifo);
        self.numbering(&perm)
    }

    pub fn bfs_numbering_src(&self, src: Node) -> Self {
        let mut bfs = BFS::new(self.n());
        let mut perm = identity_permutation(self.n());
        perm.swap(0, src as usize);
        bfs.bfs_all(self, &perm);
        assert_eq!(bfs.fifo.len(), self.n());
        let perm = inverse_permutation(&bfs.fifo);
        self.numbering(&perm)
    }

    pub fn dfs_numbering(&self) -> Self {
        let mut dfs = DFS::new(self.n());
        dfs.dfs_all(self);
        self.numbering(&dfs.number)
    }

    pub fn random_numbering(&self) -> Self {
        let perm = random_permutation(self.n());
        self.numbering(&perm)
    }

    pub fn numbering(&self, perm: &Vec<Node>) -> Self {
        assert_eq!(perm.len(), self.n());
        let mut edg = Vec::with_capacity(self.m());
        for u in 0 .. self.n() as Node {
            let ui = perm[u as usize];
            assert!(ui < self.n() as Node);
            for &v in self.neighbors(u) {
                let vi = perm[v as usize];
                assert!(vi < self.n() as Node);
                edg.push(Edge { u: ui, v: vi });
            }
        }
        Graph::from_edges(&edg, false)
    }
    
    pub fn union(&self, edg: &[Edge], shift: Node) -> Self {
        let mut edges: Vec<Edge> = Vec::with_capacity(self.m() + edg.len());
        for &e in edg {
            edges.push(Edge { u: e.u+shift, v: e.v+shift });
        }
        let mut rest = self.edges();
        edges.append(&mut rest);
        Graph::from_edges(&edges, false)
    }

}


use core::panic;
use std::fmt::Display;

impl Display for Graph {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for u in 0..self.n() as Node {
            write!(f, "{u} :")?;
            for &v in self.neighbors(u) {
                write!(f, " {v}")?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

use std::io::Error;

pub fn parse_edges<Iter>(lines: Iter) -> Vec<Edge> 
where Iter: IntoIterator<Item = Result<String, Error>>,
{
    lines.into_iter().filter(|l| -> bool {
        let l = l.as_ref().unwrap().trim();
        l.len() > 0 && ! l.starts_with('#')
    }).map(|l| -> Edge {
        l.unwrap().parse::<Edge>().unwrap()
    }).collect()
}

use std::str::FromStr;

impl FromStr for Edge {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut words = s.split_whitespace();
        let u: Node = parse_node(words.next())?;
        let v: Node = parse_node(words.next())?;
        if words.next() != None {
            Err("Too many numbers in the line.")
         } else {
            Ok(Edge { u, v })
         }
    }
}

pub fn parse_node(opt: Option<&str>) -> Result<Node, &'static str> {
    match opt {
           Some(s) => return s.parse::<Node>().map_err(|_| "bad number format for a node"),
           None => return Err("node as a number expected"),
    }
}


use rand::Rng;

pub fn erdos_renyi(n: usize, p: f64) -> Graph {
    let m = (1.1 * p * (n as f64) * (n as f64)) as usize;
    let mut edges: Vec<Edge> = Vec::with_capacity(m);
    let mut rng = rand::thread_rng();
     for u in 0..n as Node {
        // The first index of a 1 follows a geometric distribution, it can be generated by:
        let mut next_geom = || {
            (f64::ln(rng.gen::<f64>()) / f64::ln(1. - p)) as usize
        };
        let mut v = next_geom();
        while v < n {
            edges.push(Edge { u, v: v as Node });
            v += next_geom();
        }
    }
    Graph::from_edges(&edges, true)
}

pub fn stochastic_block_model(n: usize, p: f64, q: f64) -> Graph {
    let g = erdos_renyi(n, q);
    assert!(p > q);
    let p = (p - q) / (1. - q);
    let a = erdos_renyi(n/2, p).edges();
    let b = erdos_renyi(n/2, p).edges();
    g.union(&a, 0).union(&b, (n/2) as Node)
}

pub fn random_regular(n: usize, d: usize) -> Graph {
    random_fixed_degrees(vec![d; n])
}


use rand::distributions::Distribution;
use rand::distributions::WeightedIndex;

pub fn powerlaw(n: usize, beta: f64) -> Graph {
    // powerlaw distr with exponent beta:
    let mut weights = Vec::with_capacity(n);
    let nf = n as f64;
    weights.push(0.); // weight for deg = 0
    for deg in 1..n {
        weights.push(nf / f64::powf(deg as f64, beta));
    }
    let dist = WeightedIndex::new(&weights).unwrap();
    // degrees:
    let mut rng = rand::thread_rng();
    let mut degs = Vec::with_capacity(n);
    let mut m = 0;
    for _ in 0..n { 
        let d = dist.sample(&mut rng);
        degs.push(d);
        m += d; 
    }
    // half edges:
    let mut dst = Vec::with_capacity(m);
    for v in 0..n {
        for _ in 0..degs[v] {
            dst.push(v as Node);
        }
    }
    // edges:
    let mut edg = Vec::with_capacity(m);
    let perm = random_permutation(m);
    let mut e = 0 as usize;
    for u in 0..n {
        for _ in 0..degs[u] {
            edg.push(Edge { u: u as Node, v: dst[perm[e] as usize]});
            e += 1;
        }
    }
    // graph:
    Graph::from_edges(&edg, true)
}

pub fn random_fixed_degrees(degs: Vec<usize>) -> Graph {
    // Number of nodes and edges:
    let n = degs.len();
    let mut m = 0;
    for &d in &degs { 
        m += d; 
    }
    // half edges:
    let mut dst = Vec::with_capacity(m);
    for v in 0..n {
        for _ in 0..degs[v] {
            dst.push(v as Node);
        }
    }
    // edges:
    let mut edg = Vec::with_capacity(m);
    let perm = random_permutation(m);
    let mut e = 0 as usize;
    for u in 0..n {
        for _ in 0..degs[u] {
            edg.push(Edge { u: u as Node, v: dst[perm[e] as usize]});
            e += 1;
        }
    }
    // graph:
    Graph::from_edges(&edg, true)
}

pub fn random_permutation(n: usize) -> Vec<Node> {
    let mut perm = Vec::with_capacity(n);
    let mut v = 0 as Node;
    perm.resize_with(n, || { v += 1; v-1});
    let mut rng = rand::thread_rng();
    for j in (1..n).rev() {
        let i = rng.gen_range(0..=j);
        perm.swap(i, j);
    }
    perm
}

pub fn inverse_permutation(ord: &Vec<Node>) -> Vec<Node> {
    let n = ord.len() as Node;
    let mut perm = vec![n; n as usize];
    for (i, &v) in ord.iter().enumerate() {
        perm[v as usize] = i as Node;
    }
    for &v in &perm { assert!(v < n) } // check ord is a permutation 
    perm
}

pub fn identity_permutation(n: usize) -> Vec<Node> {
    let mut perm = vec![n as Node; n];
    for i in 0..n {
        perm[i] = i as Node;
    }
    perm
}


pub type Distance = u32;

pub struct BFS {
    pub dist: Vec<Distance>,
    pub fifo: Vec<Node>,
    front: usize, // index of the front of the fifo
}

impl BFS {

    const INFINITY: Distance = Distance::MAX;

    pub fn new(n: usize) -> Self {
        let dist = vec![Self::INFINITY; n];
        let fifo = Vec::with_capacity(n);
        BFS { dist, fifo, front: 0 }
    }

    pub fn clear(&mut self) {
        self.dist.fill(Self::INFINITY);
        self.fifo.clear();
        self.front = 0;
    }

    pub fn dist(&self, v: Node) -> Distance {
        self.dist[v as usize]
    }

    // return the number of nodes visited so far
    pub fn bfs<G: AGraph>(&mut self, g: &G, v: Node) -> usize {
        self.check_size(g.n());
        self.dist[v as usize] = 0;
        self.fifo.push(v);
        while self.front < self.fifo.len() {
            let v = self.fifo[self.front];
            //eprintln!("bfs: {v}");
            self.front += 1;
            let d = self.dist[v as usize] + 1;
            for &w in g.neighbors(v) {
                if self.dist[w as usize] == Self::INFINITY {
                    self.dist[w as usize] = d;
                    self.fifo.push(w);
                }
            }
        }
        self.front
    }

    pub fn bfs_all<G: AGraph>(&mut self, g: &G, perm: &Vec<Node>) {
        self.check_size(g.n());
        for &v in perm {
            if self.dist[v as usize] == Self::INFINITY {
                self.bfs(g, v);
            }
        }
    }

    fn check_size(&self, n: usize) {
        if n > self.dist.len() { 
            panic!("Graph of {} nodes is too large for this BFS struct of size {}!", 
                   n, self.dist.len()) 
        }
    }

    // Connected Components as a vector indicating beginning index in fifo.
    // The length of the vector is the number of connected components + 1.
    pub fn cc<G: AGraph>(&mut self, g: &G) -> Vec<usize> {
        self.check_size(g.n());
        let mut cc: Vec<usize> = Vec::with_capacity(g.n()+1);
        cc.push(0);
        for v in 0..g.n() as Node {
            if self.dist[v as usize] == Self::INFINITY {
                let iend = self.bfs(g, v); // end of cc of v
                cc.push(iend);
            }
        }
        cc
    }

    pub fn largest_cc(&self, cc : &Vec<usize>) -> &[Node] {
        let mut c_max = 0;
        let mut sz_max = cc[c_max+1];
        for c in 1 .. cc.len() - 1 {
            let sz = cc[c+1] - cc[c];
            if sz > sz_max {
                sz_max = sz;
                c_max = c;
            }
        }
        let b = cc[c_max];
        let e = cc[c_max+1];
        &self.fifo[b..e]
    }

}


pub struct DFS {
    stack: Vec<Node>,
    pub number: Vec<Node>,
    num: usize,
}

impl DFS {

    const INFINITY: Node = Node::MAX;

    pub fn new(n: usize) -> Self {
        let stack = Vec::with_capacity(n);
        let number = vec![Self::INFINITY; n];
        DFS { stack, number, num: 0 }
    }

    pub fn clear(&mut self) {
        self.stack.clear();
        self.number.fill(Self::INFINITY);
        self.num = 0;
    }

    fn check_size(&self, n: usize) {
        if n > self.number.len() { 
            panic!("Graph of {} nodes is too large for this DFS struct of size {}!", 
                   n, self.number.len()) 
        }
    }

     pub fn dfs<G: AGraph>(&mut self, g: &G, u: Node) {
        self.stack.push(u);
        while let Some(u) = self.stack.pop() {
            if self.number[u as usize] == Self::INFINITY {
                self.number[u as usize] = self.num as Node;
                self.num += 1;
                for &v in g.neighbors(u) {
                    if self.number[v as usize] == Self::INFINITY {
                        self.stack.push(v)
                    } 
                }
            }
        }
    }

    pub fn dfs_all<G: AGraph>(&mut self, g: &G) {
        self.check_size(g.n());
        for v in 0..g.n() as Node {
            if self.number[v as usize] == Self::INFINITY {
                self.dfs(g, v);
            }
        }
    }

}

pub mod graphs {

    use super::*;

    use std::cmp::max;

    /// Generate edges of a path of length `len` from `src` to `dst`.
    /// Intermediate nodes are numbered starting from `n` (`src` and `dst` are avoided).
    /// Returns the edges and the next free number for more futur nodes.
    pub fn e_path(len: usize, src: Node, dst: Node, n: Node) -> (Vec<Edge>, Node) {
        if len == 0 { self::panic!("A path has length at least 1 even if `src == dst`."); }
        let mut edges: Vec<Edge> = Vec::with_capacity(len);
        if len == 1 {
            edges.push(Edge { u: src, v: dst });
            (edges, max(n, max(src, dst) + 1))
        } else {
            let mut n = n;
            let mut next = || {
                if n == src { n += 1 }
                if n == dst { n += 1 }
                let v = n;
                n += 1;
                v 
            };
            let mut u = src;
            let mut v = u;
            for _ in 0..len-1 {
                v = next();
                edges.push(Edge { u, v });
                u = v;
            }
            edges.push(Edge { u: v, v: dst });
            (edges, next())
        }
    }

    pub fn path(len: usize) -> Graph {
        Graph::from_edges(&e_path(len, 0, len as Node, 0).0, true)
    }

}


#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn check_path() {
        let (p1, n) = graphs::e_path(1, 0, 1, 0);
        assert_eq!(n, 2);
        assert_eq!(p1, [Edge {u: 0, v: 1}]);
        let (p3, n) = graphs::e_path(3, 0, 1, n);
        println!("n={n} p3: {:?}", p3);
        assert_eq!(n, 4);
        assert_eq!(* p3.last().unwrap(), Edge {u: 3, v: 1});
        let (p3, n) = graphs::e_path(3, 0, 3, 0);
        println!("n={n} p3: {:?}", p3);
        assert_eq!(n, 4);
    }

    #[test]
    fn check_graph() {
        let g = Graph::from_edges(&[Edge {u: 0, v: 1}, Edge {u: 1, v: 2}], true);
        for u in 0..g.n() as Node {
            for &v in g.neighbors(u) {
                println!("{u} -- {v}");
            }
        }
        assert_eq!( * g.neighbors(1).first().unwrap(), 0);
        assert_eq!( * g.neighbors(1).last().unwrap(), 2);
    }

    #[test]
    fn check_bfs() {
        let (p3, n) = graphs::e_path(3, 0, 3, 0);
        println!("n={n} p3: {:?}", p3);
        let (p5, n) = graphs::e_path(5, n, n+5, n);
        let mut edg = p3.clone();
        edg.extend(&p5);

        let g = Graph::from_edges(&edg, true);
        println!("n={n} g: {:?}", g.edges_undirected());
        let mut bfs = BFS::new(2*g.n());
        let vis = bfs.bfs(&g, 2);
        assert_eq!(vis, p3.len()+1);
        let g_lcc = g.largest_cc();
        println!("g_lcc:\n{}", g_lcc);
        assert_eq!(g_lcc.n(), p5.len()+1);

        edg.push(Edge {u: 1, v: 6});
        edg.push(Edge {u: 10, v: 11}); // isolated edge
        let n = 12;
        let g = Graph::from_edges(&edg, true);
        bfs.clear();
        bfs.bfs(&g, 8);
        assert_eq!(bfs.dist(8), 0);
        assert_eq!(bfs.dist(0), 4);
        assert_eq!(bfs.dist(2), 4);
        assert_eq!(bfs.dist(4), 4);
        assert_eq!(bfs.dist(3), 5);
        assert_eq!(bfs.dist(11), BFS::INFINITY);

        let (p5, n) = graphs::e_path(2, 2, 9, n);
        edg.extend(&p5);
        let g = Graph::from_edges(&edg, true);
        bfs.clear();
        bfs.bfs(&g, 8);
        assert_eq!(bfs.dist(3), 4);

        println!("n={n}")
    }


    use std::cmp;

    impl DFS {
            // return lowpointer (see Tarjan's strong cc algorithm)
            // cannot handle big graphs (stack overflow)
            fn dfs_rec<G: AGraph>(&mut self, g: &G, u: Node) -> Node {
                self.number[u as usize] = self.num as Node;
                let mut lowptr = self.num as Node;
                self.num += 1;
                for &v in g.neighbors(u).iter().rev() { // to compare with dfs()
                    lowptr = cmp::min(lowptr, 
                        if self.number[v as usize] == Self::INFINITY {
                            self.dfs_rec(g, v)
                        } else {
                            self.number[v as usize]
                        }
                    )
                }
                lowptr
            }
    }

    #[test]
    fn check_dfs() {
        let (p3, n) = graphs::e_path(3, 0, 3, 0);
        println!("n={n} p3: {:?}", p3);
        let (p5, n) = graphs::e_path(5, n, n+5, n);
        let mut edg = p3.clone();
        edg.extend(&p5);

        let g = Graph::from_edges(&edg, true);
        println!("n={n} g: {:?}", g.edges_undirected());
        let mut dfs = DFS::new(2*g.n());
        let lowpointer = dfs.dfs_rec(&g, 2);
        println!("lowpointer(2)={lowpointer}");
    }

}
