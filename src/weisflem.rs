use crate::graph::*;
use crate::linegraph::*;
use crate::partition::*;
pub type Color = Node;

pub struct Coloring {
    // node colors
    pub col : Vec<Color>,
    // colors definition
    pub col_def : Graph,
    // colors definition extension (see refine_edge)
    pub col_def_snd : Graph,
    // partition according to colors
    pub ptt: Partition,
}

use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Bicolor {
    fst: Color,
    snd: Color,
}

use std::iter::zip;

impl Coloring {

    pub fn new(n: usize) -> Self {
        let col: Vec<Color> = vec![0; n]; // all have color 0 which has empty definition
        Coloring { col, col_def: Graph::empty(1), col_def_snd: Graph::empty(1), ptt: Partition::new(n, false) }
    }

    // [n] nodes, [nc] colors, [color_of(u)] returns the color of [u].
    pub fn from_fn<F>(n: usize, nc: usize, color_of: F) -> Self 
    where F: Fn(Node) -> Color
    {
        let mut col: Vec<Color> = vec![0; n];
        for u in 0..n as Node {
            col[u as usize] = color_of(u);
        }
        Coloring { col, col_def: Graph::empty(nc), col_def_snd: Graph::empty(nc), ptt: Partition::new(n, false) }
    }

    fn refine<G: AGraph>(&mut self, g: &G) -> bool {
        assert_eq!(self.col.len(), g.n());
        // Colors from neighborhood
        let mut cols: Vec<Edge>  = Vec::with_capacity(g.m());
        for v in 0..g.n() as Node {
            for &w in g.neighbors(v) {
                cols.push(Edge { u: self.col[w as usize], v})
            }
        }
        let col_to_nodes = Graph::from_edges(&cols, false);
        self.refine_col(&col_to_nodes)
    }

    fn refine_col<G: AGraph>(&mut self, col_to_nodes: &G) -> bool {

        let nc = self.ptt.len(); // number of color classes
        
        // split color classes (main loop):
        for c in 0..nc as Color {
            //eprintln!("c={c}: {:?}", col_to_nodes.neighbors(c));
            self.ptt.split_left_multi(col_to_nodes.neighbors(c));
            //eprintln!("c={c}: {}", self.ptt);
        }

        // Redefine colors
        //self.ptt.renumber_parts();
        let node_to_cols = Graph::from_edges(&col_to_nodes.edges_reversed(), false); // sorted adjacency lists
        let mut col_def: Vec<Edge>  = Vec::with_capacity(node_to_cols.m());
        for (c, pt) in self.ptt.parts.iter().enumerate() {
            let c = c as Color;
            assert!(pt.left < pt.right);
            let first = self.ptt.elements[pt.left];
            for &d in node_to_cols.neighbors(first) {
                col_def.push(Edge {u: c, v: d });
            }
            for &v in &self.ptt.elements[pt.left .. pt.right] {
                self.col[v as usize] = c;
            }
        }
        self.col_def = Graph::from_edges(&col_def, false);

        //eprintln!("{}", self.ptt);
        //eprintln!("{:?}", self.col_def);

        // Return true if some part has been split
        self.ptt.len() > nc
    }

    fn refine_edge(&mut self, g: &LGraph, head: bool) -> bool {

        assert_eq!(self.col.len(), g.n());

       // Colors from neighborhood
        let mut cols: Vec<Edge>  = Vec::with_capacity(g.m());
        for a in 0..g.n() as Arc {
            if head {
                let v = g.head(a);
                for &b in g.out_arcs(v).iter() {
                    cols.push(Edge { u: self.col[b as usize], v: a})
                }
            } else {
                let u = g.tail(a);
                for &b in g.in_arcs(u).iter() {
                    cols.push(Edge { u: self.col[b as usize], v: a})
                }
            }
        }
        let col_to_nodes = Graph::from_edges(&cols, false);
        self.refine_col(&col_to_nodes)
    }

    #[allow(dead_code)]
    fn refine_edge_bidir(&mut self, g: &LGraph, head: bool) -> bool {

        assert_eq!(self.col.len(), g.n());

        let nc = self.ptt.len(); // number of color classes

        // Index color pairs
        let mut bicolors = Vec::with_capacity(g.n());
        let mut index = HashMap::with_capacity(g.n());
        let mut node_of_bicolor = |fst, snd| -> Node {
            let b = Bicolor { fst, snd };
            match index.get(&b) {
                Some(v) => *v,
                None => { 
                    let v = bicolors.len() as Node;
                    bicolors.push(b.clone()); 
                    index.insert(b, v); 
                    v 
                }  
            }
        };
        
        // Colors from neighborhood
        let mut cols: Vec<Edge>  = Vec::with_capacity(g.m());
        for a in 0..g.n() as Arc {
            if head {
                let v = g.head(a);
                for (&b, &c) in zip(g.out_arcs(v).iter(), 
                                              g.in_arcs(v).iter()) {
                    let bic = node_of_bicolor(self.col[b as usize], self.col[c as usize]);
                    cols.push(Edge { u: bic, v: a})
                }
            } else {
                let u = g.tail(a);
                for (&b, &c) in zip( g.in_arcs(u).iter(), 
                                               g.out_arcs(u).iter()) {
                    let bic = node_of_bicolor(self.col[b as usize], self.col[c as usize]);
                    cols.push(Edge { u: bic, v: a})
                }
            }
        }
        let col_to_nodes = Graph::from_edges(&cols, false);

        // split color classes (main loop):
        for c in 0..nc as Color {
            //eprintln!("c={c}: {:?}", col_to_nodes.neighbors(c));
            self.ptt.split_left_multi(col_to_nodes.neighbors(c));
            //eprintln!("c={c}: {}", self.ptt);
        }

        // Redefine colors
        //self.ptt.renumber_parts();
        let node_to_cols = col_to_nodes.reverse();
        let mut col_def: Vec<Edge>  = Vec::with_capacity(node_to_cols.m());
        let mut col_def_snd: Vec<Edge>  = Vec::with_capacity(node_to_cols.m());
        for (c, pt) in self.ptt.parts.iter().enumerate() {
            let c = c as Color;
            assert!(pt.left < pt.right);
            let first = self.ptt.elements[pt.left];
            for &bic in node_to_cols.neighbors(first) {
                let Bicolor { fst, snd } = bicolors[bic as usize];
                col_def.push(Edge {u: c, v: fst });
                col_def_snd.push(Edge {u: c, v: snd });
            }
            for &a in &self.ptt.elements[pt.left .. pt.right] {
                self.col[a as usize] = c;
            }
        }
        self.col_def = Graph::from_edges(&col_def, false);
        self.col_def_snd = Graph::from_edges(&col_def_snd, false);

        //eprintln!("{:?}", self.col);
        //eprintln!("{:?}", self.col_def);
        //eprintln!("{:?}", self.col_def_snd);

        // Return true if some part has been split
        self.ptt.len() > nc
    }

}

pub fn wl1<G: AGraph>(g: &G) {
    let mut ptt = Partition::new(g.n(), true);
    ptt.refine_neighbors_multi(g);
    if ptt.len() < 50 { eprintln!("{}", ptt); }
    eprintln!("WL1: {} colors for {} nodes.", ptt.len(), g.n());
}

pub fn wl1_iso<G: AGraph>(g: &G, h: &G) -> bool {
    let mut ptt_g = Partition::new(g.n(), true);
    ptt_g.refine_neighbors_multi(g);
    eprintln!("WL1: {} colors for {} nodes.", ptt_g.len(), g.n());
    let mut ptt_h = Partition::new(h.n(), true);
    ptt_h.refine_neighbors_multi(h);
    eprintln!("WL1: {} colors for {} nodes.", ptt_h.len(), h.n());
    if ptt_g.len() != ptt_h.len() {
        eprintln!("different number of colors !!!"); return false
    }
    for p in 0..ptt_g.len() {
        let pt_g = &ptt_g.parts[p];
        let pt_h = &ptt_h.parts[p];
        if pt_g.len() != pt_h.len() {
            eprintln!("different number of nodes of color {p} !!!"); return false
        }
    }
    if ptt_g.part_graph(g) != ptt_h.part_graph(h) {
        eprintln!("different color graphs !!!"); return false
    }
    true
}


pub fn wl1_col<G: AGraph>(g: &G) {
    let mut col = Coloring::new(g.n());
    let mut it = 0;
    while col.refine(g) { it += 1; }
    eprintln!("WL1: {it} iterations led to {} colors for {} nodes.", col.ptt.len(), g.n());
}

pub fn wl1_iso_col<G: AGraph>(g: &G, h: &G) -> bool {
    let mut c_g = Coloring::new(g.n());
    let mut c_h = Coloring::new(h.n());
    let mut it = 0;
    while c_g.refine(g) {
        if ( ! c_h.refine(h)) || c_g.col_def != c_h.col_def { eprintln!("different colors !!!"); return false }
        assert_eq!(c_g.ptt.len(), c_h.ptt.len());
        for p in 0..c_g.ptt.len() {
            if c_g.ptt.parts[p].len() != c_h.ptt.parts[p].len() { eprintln!("different parts !!!"); return false }
        }
        it += 1; 
    }
    eprintln!("WL1: {it} iterations led to {} colors for {} nodes.", c_g.ptt.len(), g.n());
    true
}


pub fn wl_edge<G: AGraph>(g: &G) {
    let g = LGraph::from_edges(&g.edges_undirected(), true);
    let is_loop = |a| if g.tail(a) == g.head(a) {0} else {1};
    let mut col = Coloring::from_fn(g.n(), 2, is_loop);
    let mut it = 0;
    while col.refine_edge(&g, true) || col.refine_edge(&g, false) { it += 1; }
    eprintln!("WL-edge: {it} iterations led to {} colors for {} nodes.", col.ptt.len(), g.n());
    for u in 0..g.n_graph() as Node {
        print!("{u} :");
        for &a in g.out_arcs(u) {
            print!(" {},{}", col.col[a as usize], col.col[g.rev_arc(a) as usize]);
        }
        println!("");
    }
}

pub fn wl_edge_iso<G: AGraph>(g: &G, h: &G) -> bool {
    let g = LGraph::from_edges(&g.edges_undirected(), true);
    let h = LGraph::from_edges(&h.edges_undirected(), true);
    let is_loop_g = |a| if g.tail(a) == g.head(a) {0} else {1};
    let mut c_g = Coloring::from_fn(g.n(), 2, is_loop_g);
    let is_loop_h = |a| if h.tail(a) == h.head(a) {0} else {1};
    let mut c_h = Coloring::from_fn(h.n(), 2, is_loop_h);
    let mut it = 0;
    let mut iso = true;
    let mut iter = |head: bool| -> bool {
        if ! iso { return false } // may save a useless pass
        let r_g = c_g.refine_edge(&g, head);
        let r_h = c_h.refine_edge(&h, head);
        if r_g != r_h || c_g.col_def != c_h.col_def || c_g.col_def_snd != c_h.col_def_snd { 
            eprintln!("different colors !!!"); 
            iso = false;
            return false 
        }
        eprintln!("Nb colors: {} {}", c_g.col_def.n(), c_h.col_def.n());
        assert_eq!(c_g.ptt.len(), c_h.ptt.len());
        for p in 0..c_g.ptt.len() {
            if c_g.ptt.parts[p].len() != c_h.ptt.parts[p].len() { 
                eprintln!("different parts !!!"); 
                iso = false;
                return false 
            }
        }
        r_g
    };
    while iter(true) || iter(false) {
        it += 1; 
    }
    if iso {
        eprintln!("WL-edge-iso: {it} iterations led to {} colors for {} nodes.", c_g.ptt.len(), g.n());
    }
    iso
}


