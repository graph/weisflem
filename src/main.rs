pub mod index;
pub mod graph;
pub mod dygraph;
pub mod linegraph;
pub mod partition;
pub mod weisflem;
pub mod modules;

use graph::*;
use dygraph::Dygraph;
use linegraph::LGraph;
use partition::{graph_core, graph_core_ordering, Partition};
use modules::*;

use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader, BufWriter, StdoutLock, Write};
use std::process::ExitCode;
use rand::Rng;

fn main() -> ExitCode {
    let mut args: Vec<String> = env::args().collect();
   
    if args.len() < 2 || args[1].eq("-h") || args[1].eq("--help") {
        panic!("\n\
Usage : {} [command] [...]

Apply various partition refinement techniques based on the possible
following [command]s:

wl1 [fname]
Reads edges from file [fname] (or stdin if [fname] is [-])
formatted as one per line as [u v] where [u] and [v] are two integers.
Computes WL1 color classes on the resulting (undirected) graph.

wl1iso [fname1] [fname2] 
Reads edges from file [fname1] (or stdin if [fname1] is [-])
formatted as one per line as [u v] where [u] and [v] are two integers.
Similarly read [fname2].
Run WL1 isomorphism test on both resulting (undirected) graphs.

lexbfs [fname] [src]
Reads edges from [fname] similarly as above, and performs a lexicographic BFS
from source vertex [src] in the resulting (undirected) graph.

bfs [fname] [src]
Same as lexbfs with a classical BFS.

low-crossing [fname]
Reads edges from [fname] similarly as above, and output a path v1,...,vn such
that closed neighborhoods of the resulting (undirected) graph cross few edges.
More precisely, if the graph has VC-dimension d, the overall number of crossings
is O(n^(2-1/d)) with high probability (inspired from [Duraj, Konieczny, Potepa, 2023]). 

low-crossing-dir [fname]
Same as low-crossing with a directed graph and open out-neighborhoods.

wl1er [n] [d]
Construct an Erdos-Renyi graph G_n,d/n with [n] vertices and probability
[p=d/n] for edge presence and compute color classes according to WL1, both
on the graph and on its 2-core. 

speed [n] [beta]
Construct a random power-law graph with [n] vertices and law exponent [beta],
and times various basic graph procedures on it (BFS, DFS, WL1).

core-ordering [fname]
Reads edges from [fname] similarly as above, and output a core elemination ordering.

factor-perm [fname]
Reads edges from [fname] similarly as above, and output a factorizing permutation
for strong modules, i.e. an ordering of the nodes of the input graph such that
any strong module (a module that do not overlap any other module) is a factor.

modular-decomposition [fname]
Reads edges from [fname] similarly as above, and output the tree of the modular 
decomposition, using parenthesis to delimit sub-trees. Children are sorted by 
increasing ID of the descendant with smallest ID.
", &args[0]);
    }

    // -------------------- arguments -----------------------
    args.reverse(); // use it as a stack
    args.pop(); // command name
    let mut graphs: Vec<Graph> = Vec::new(); // a stack of graphs
    
while ! args.is_empty() {

    let command = args.pop().unwrap();

    match command.as_str() {

        "pushgraph" | "pg" => {
            let gen = args.pop().unwrap();
            let g = match gen.as_str() {
                "er" => {
                    let n: usize = args.pop().unwrap().parse().unwrap();
                    let p: f64 = args.pop().unwrap().parse::<f64>().unwrap() / (n as f64);
                    erdos_renyi(n, p)
                },
                "rr" => {
                    let n: usize = args.pop().unwrap().parse().unwrap();
                    let d: usize = args.pop().unwrap().parse().unwrap(); // degree
                    random_regular(n, d)
                },
                "f" => read_graph(args.pop().unwrap(), true),
                "fdir" => read_graph(args.pop().unwrap(), false),
                _ => panic!("Unkown graph generator '{}'.", gen)
            };
            eprintln!("Graph with {} nodes and {} edges.", g.n(), g.m());
            graphs.push(g);
        },

        "cc" => {
            let g = graphs.pop().unwrap();
            let g = g.largest_cc();
            graphs.push(g);
        },

        "wl1" | "WL1" => { // ------------- Weisfeiler Leman -----------------
            let g = read_graph(args.pop().unwrap(), true);
            weisflem::wl1(&g);
            let c = g.largest_cc();
            eprintln!("Largest component has {} nodes and {} edges.", c.n(), c.m());
            let c = graph_core(&c, 2);
            eprintln!("Core of largest component has {} nodes and {} edges.", c.n(), c.m());
            weisflem::wl1(&c);
            //weisflem::wl_edge(&g);
        },

        "wl1iso" | "WL1iso" => { // ------------- Weisfeiler Leman -----------------
            let g = read_graph(args.pop().unwrap(), true);
            weisflem::wl1(&g);
            let lg = LGraph::from_edges(&g.edges(), true);
            eprintln!("Line graph has {} nodes and {} edges.", lg.n(), lg.m());
            weisflem::wl1(&lg);
            weisflem::wl_edge(&g);
            let h = read_graph(args.pop().unwrap(), true);
            weisflem::wl1(&h);
            weisflem::wl_edge(&h);
            println!("WL1: {}", weisflem::wl1_iso(&g, &h));
            println!("WL_edge: {}", weisflem::wl_edge_iso(&g, &h));
        },

        "lexbfs" => { // ------------- Lexicographic BFS -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let src: Node = args.pop().unwrap().parse().unwrap();
            let ord = partition::lexbfs(&g, src);
            for v in ord {
                println!("{v}")
            }
        },

        "bfs" => { // ------------- Lexicographic BFS -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let src: Node = args.pop().unwrap().parse().unwrap();
            let mut idty = vec![0 as Node; g.n()];
            for i in 0..g.n() { idty[i] = i as Node }
            idty.swap(0, src as usize);
            let mut bfs = BFS::new(g.n());
            bfs.bfs_all(&g, &idty);
            for v in bfs.fifo {
                println!("{v}")
            }
        },

        "low-crossing" | "low-stabbing" | "lowcross" | "lowstab" => { 
            // A path v1,...vn such that neighborhoods cross few edges on average.
            let g = read_graph(args.pop().unwrap(), true);
            // add loops:
            let mut edg = g.edges();
            for v in 0..g.n() as Node { edg.push(Edge { u: v, v }); }
            let g = Graph::from_edges(&edg, false);
            let ord = partition::low_stabbing_ordering(&g);
            for v in ord {
                println!("{v}")
            }
        },

        "low-crossing-dir" | "lowcrossdir" => { 
            // A path v1,...vn such that neighborhoods cross few edges on average.
            let g = read_graph(args.pop().unwrap(), false);
            let ord = partition::low_stabbing_ordering(&g);
            for v in ord {
                println!("{v}")
            }
        },

        "low-crossing-pairs" => { 
            // For each u pairs uv_1,...uv_d with potentially low crossing number.
            let g = read_graph(args.pop().unwrap(), true);
            let deg: usize = args.pop().unwrap().parse().unwrap();
            assert!(deg <= g.n());
            // add loops:
            let mut edg = g.edges();
            for v in 0..g.n() as Node { edg.push(Edge { u: v, v }); }
            let g = Graph::from_edges(&edg, false);
            let mut ptt = Partition::new(g.n(), false);
            let perm = random_permutation(g.n());
            for i in 0..g.n() {
                ptt.swap(i, ptt.pos_of[perm[i] as usize]);
            }
            for u in 0..g.n() as Node {
                ptt.clear(); // TODO permute nodes
                for i in 0..deg {
                    let r = u as usize + 19; // pseudo random
                    ptt.swap(i, ptt.pos_of[perm[(i + r*r) % g.n()] as usize]);
                }
                for &v in g.neighbors(u) { // TODO permute neighbors
                    ptt.split_left(g.neighbors(v));
                }
                for i in 1..=deg {
                    println!("{u} {}", ptt.elements[i])
                }
            }
        },
        
        "lexbfs-numbering-dir" => { // ------------- Lexicographic BFS -----------------
            let g = read_graph(args.pop().unwrap(), false);
            let src: Node = args.pop().unwrap().parse().unwrap();
            let ord = partition::lexbfs(&g, src);
            let perm = inverse_permutation(&ord);
            let g = g.numbering(&perm);
            for u in 0..g.n() as Node {
                for &v in g.neighbors(u) { println!("{u} {v}") }
            }
        },

        "bfs-numbering-dir" => { // -------------  BFS -----------------
            let g = read_graph(args.pop().unwrap(), false);
            let src: Node = args.pop().unwrap().parse().unwrap();
            let g = g.bfs_numbering_src(src);
            for u in 0..g.n() as Node {
                for &v in g.neighbors(u) { println!("{u} {v}") }
            }
        },

        "dfs-numbering-dir" => { // -------------  BFS -----------------
            let g = read_graph(args.pop().unwrap(), false);
            //let src: Node = args.pop().unwrap().parse().unwrap();
            let g = g.dfs_numbering();
            for u in 0..g.n() as Node {
                for &v in g.neighbors(u) { println!("{u} {v}") }
            }
        },

        "low-crossing-numbering-dir" => { // see low-crossing-dir 
            let g = read_graph(args.pop().unwrap(), false);
            let ord = partition::low_stabbing_ordering(&g);
            let perm = inverse_permutation(&ord);
            let g = g.numbering(&perm);
            for u in 0..g.n() as Node {
                for &v in g.neighbors(u) { println!("{u} {v}") }
            }
        },

        "er" | "ER" => { // ------------------ Erdos Renyi -------------------
            let n: usize = args.pop().unwrap().parse().unwrap();
            let p: f64 = args.pop().unwrap().parse::<f64>().unwrap() / (n as f64);
            let er = erdos_renyi(n, p);
            //let er = powerlaw(n, 2.2);
            eprintln!("Erdos Renyi with {} nodes and {} edges.", er.n(), er.m());
            weisflem::wl1(&er);
            let er = er.largest_cc();
            eprintln!("Erdos Renyi largest component has {} nodes and {} edges.", er.n(), er.m());
            let er = graph_core(&er, 2);
            eprintln!("Core largest component has {} nodes and {} edges.", er.n(), er.m());
            weisflem::wl1(&er);
        },

        "perturb" => { // --------------------- Perturb a graph --------------------
            let k: usize = args.pop().unwrap().parse().unwrap();
            let g = graphs.pop().unwrap();
             weisflem::wl1(&g);
            // Add k random edges:
            let mut rng = rand::thread_rng();
            let n = g.n();
            let mut g = Dygraph::from_edges(&g.edges(), false);
            for _ in 0..k {
                let u = rng.gen_range(0..n) as Node;
                let v = rng.gen_range(0..n) as Node;
                g.add_arc(u, v);
                g.add_arc(v, u);
            }
            eprintln!("Perturbed graph has {} nodes and {} edges.", g.n(), g.m());
            weisflem::wl1(&g);
        }

        "speed" => { // ------------------ Speed test -------------------
            let n: usize = args.pop().unwrap().parse().unwrap();
            let beta: f64 = args.pop().unwrap().parse().unwrap();
            let g = powerlaw(n, beta);
            eprintln!("Graph with {} nodes and {} edges.", g.n(), g.m());
            let g = g.dfs_numbering();
            //let g = g.numbering(& partition::low_stabbing_ordering(&g));
            eprintln!("Renumbered graph.");
            let edg = g.edges();
            let d = Dygraph::from_edges(&edg, false);
            eprintln!("Dygraph with {} nodes and {} edges", d.n(), d.m());
            
            let mut bfs = BFS::new(g.n());
            let s = g.n() as Node / 2;
            timeit(10, "BFS Dygraph",
                || { bfs.bfs(&d, s); bfs.clear() });
            timeit(10, "BFS Graph",
                ||  { bfs.bfs(&g, s); bfs.clear() });

            let mut dfs = DFS::new(g.n());
            timeit(10, "DFS Dygraph",
                || { dfs.dfs(&d, s); dfs.clear() });
            timeit(10, "DFS Graph",
                ||  { dfs.dfs(&g, s); dfs.clear() });

            timeit(1, "WL1 Dygraph", || weisflem::wl1(&d));
            timeit(1, "WL1 Graph",|| weisflem::wl1(&g));

            let g = g.largest_cc();
            let g = graph_core(&g, 2);
            eprintln!("Largest component has {} nodes and {} edges.", g.n(), g.m());
            weisflem::wl1(&g);
        },

        "speed-sort-graph" => { // ---------- speed of sorting graph -----------------
            let edg_orig = read_edges(args.pop().unwrap());
            let incr_fact: usize = 10;
            let n = 1 + edg_orig.iter().map(|e| e.max()).max().unwrap();
            let mut edg = Vec::with_capacity(edg_orig.len() * incr_fact);
            timeit(1, "incr size", || {
                let perm = random_permutation(incr_fact);
                for i in 1..incr_fact as usize {
                    let u_offset = perm[i-1] * n;
                    let v_offset = perm[i] * n;
                    for e in &edg_orig {
                        edg.push(Edge { u: u_offset + e.u, v: v_offset + e.v })
                    }
                }
            });
            let edg_rev: Vec<Edge> = edg.iter().map(|e| e.rev()).collect();
            eprintln!("{} nodes {} edges", n * incr_fact as Node, edg.len());
            let nb_it = 10;
            timeit(nb_it*1000, "scan of edg", || edg.iter().map(|e| e.max()).max().unwrap());
            timeit(nb_it, "graph of edg", || Graph::from_edges(&edg, true));
            timeit(nb_it, "graph of edg rev", || Graph::from_edges(&edg_rev, true));
            timeit(nb_it, "alloc rev dygraph", || { 
                Dygraph::from_edges(&edg_rev, false)
            });
            timeit(nb_it, "edge alloc", || edg.clone());
            timeit(nb_it, "edge alloc + sort by src", || {
                let mut edg = edg.clone(); 
                edg.sort_unstable_by_key(|&e| e.u)
            });
            timeit(nb_it, "edge alloc + sort by dst", || {
                let mut edg = edg.clone(); 
                edg.sort_unstable_by_key(|&e| e.v)
            });
            timeit(nb_it, "edge alloc + sort by key", || {
                let mut edg = edg.clone(); 
                edg.sort_unstable_by_key(|&e| (e.u, e.v))
            });
            timeit(nb_it, "edge alloc + rev sort by key", || {
                let mut edg = edg.clone(); 
                edg.sort_unstable_by_key(|&e| (e.v, e.u))
            });
            timeit(nb_it, "edge alloc + rev sort", || {
                let mut edg = edg.clone(); 
                edg.sort_unstable_by(|&e, &f|
                    if e.v == f.v { e.u.cmp(&f.u) }
                    else { e.v.cmp(&f.v) }
                )
            });
        }

        "core-ordering" => { // ------------- Factorizing permutation -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let perm = graph_core_ordering(&g);
            for v in perm { println!("{v}") }
        },

        "factor-perm" => { // ------------- Factorizing permutation -----------------
            let g = read_graph(args.pop().unwrap(), true);
            //let mut g = Dygraph::from_agraph(&g);
            //g.sort();
            let perm = factor_perm(&g);
            for v in perm { println!("{v}") }
        },

        "splitters" => { // ------------- Factorizing permutation -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let mut g = Dygraph::from_agraph(&g);
            g.sort();
            let u: Node = args.pop().unwrap().parse().unwrap();
            let v: Node = args.pop().unwrap().parse().unwrap();
            print!("splitters for {u} {v}: ");
            g.adj_diff_iter(u, v, |s| print!("{s}, "));
            println!("")
        },

        "largest-cc" => { // ------------- Factorizing permutation -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let g = g.largest_cc();
            eprintln!("Largest component has {} nodes and {} edges.", g.n(), g.m());
            for u in 0..g.n() as Node {
                for &v in g.neighbors(u) {
                    if u < v { println!("{u} {v}") }
                }
            }
        },

        "modular-decomposition" | "modular-dec" | "mod-dec" => { // ------------- Factorizing permutation -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let perm = factor_perm(&g);
            //eprintln!("perm = {:?}", &perm);
            let td = TreeDec::from_factor_perm(&g, perm);
            let (smodules, tree) = (td.smodules, td.tree);
            let root: Node = 0;
            let mut leaves = 0;
            for &i in tree.neighbors(root) { if tree.deg(i) == 0 { leaves += 1 } }
            eprintln!("root has {} children ({} are leaves)", tree.deg(0), leaves);
            //
            fn subtree_size(t: &Dygraph, i: Node) -> usize {
                if t.deg(i) == 0 { return 1 } // leaf node
                let mut s: usize = 0;
                for &j in t.neighbors(i) { s+= subtree_size(t, j) }
                s
            }
            assert_eq!(subtree_size(&tree, root), g.n());
            fn largest_child(tree: &Dygraph, root: Node) -> (Node, usize) {
                let mut imax = root;
                let mut smax: usize = 0;
                for  &i in tree.neighbors(root) {
                    let s = subtree_size(&tree, i);
                    if s > smax {
                        imax = i;
                        smax = s;
                    }
                }
                (imax, smax)
            }
            let (imax, smax) = largest_child(&tree, root);
            eprintln!("largest child({imax}) has size {smax}");
            let mut leaves = 0;
            for &i in tree.neighbors(imax) { if tree.deg(i) == 0 { leaves += 1 } }
            eprintln!("child({imax}) has {} children ({} are leaves)", tree.deg(imax), leaves);
            let (_, smax) = largest_child(&tree, imax);
            eprintln!("  its largest child has size {smax}");
            //
            fn smallest_leaf(smodules: &Vec<(Node, Node)>, perm: &Vec<Node>, 
                             t: &Dygraph, i: Node) -> Node {
                if t.deg(i) == 0 { return perm[smodules[i as usize].0 as usize] } // leaf node
                let mut s: Node = Node::MAX;
                for &j in t.neighbors(i) { 
                    let l = smallest_leaf(smodules, perm, t, j);
                    if l < s { s = l }
                }
                s
            }
            fn write_tree(smodules: &Vec<(Node, Node)>, perm: &Vec<Node>, 
                          t: &Dygraph, i: Node, f: &mut BufWriter<StdoutLock<'static>>) {
                if t.deg(i) == 0 { 
                    let l = perm[smodules[i as usize].0 as usize];
                    write!(f, "{l}").expect("can write");
                } else {
                    write!(f, "(").expect("can write");
                    let mut fst = true;
                    let mut children = t.neighbors(i).to_vec();
                    children.sort_unstable_by_key(|&j| smallest_leaf(smodules, perm, t, j));
                    //let (l, r) = smodules[i as usize];
                    //eprintln!("strong module perm[{l}..={r}] = {:?} deg={}", 
                    //          &perm[l as usize ..= r as usize], t.deg(i as Node));
                    for j in children { 
                        if fst { fst = false } else { write!(f, "\n").expect("can write") }
                        write_tree(smodules, perm, t, j, f);
                    }
                    write!(f, ")").expect("can write");
                }
            }
            // let file = File::create("/tmp/tree")
            //     .expect("Unable to create file /tmp/tree");
            // let mut f = BufWriter::new(file);
            let stdout = std::io::stdout();
            let lock = stdout.lock();
            let mut out = BufWriter::new(lock);
            write_tree(&smodules, &td.perm, &tree, root, &mut out);
        },

        "factor-perm-check" => { // ------------- Factorizing permutation -----------------
            let g = read_graph(args.pop().unwrap(), true);
            let c = g.largest_cc();
            eprintln!("Largest component has {} nodes and {} edges.", c.n(), c.m());
            let perm = factor_perm(&c);
            check_two_modules(&c, &perm);
        },

        _ => panic!("Unkown command '{}'.", command)
    }

}

    ExitCode::from(0)
}

fn read_edges(fname: String) -> Vec<Edge> {
    let edges = if fname.eq("-") {
        graph::parse_edges(BufReader::new(io::stdin()).lines())
    } else {
        let file = File::open(fname)
            .expect("Should have been able to read the file");
        graph::parse_edges(BufReader::new(file).lines())
    };
    eprintln!("Read {} edges.", edges.len());
    edges
}

fn read_graph(fname: String, undir: bool) -> Graph {
    let edges = read_edges(fname);
    let g = Graph::from_edges(&edges, undir);
    eprintln!("Graph with {} nodes and {} adjacencies.", g.n(), g.m());

    g
}


use std::time::SystemTime;

fn timeit<F: FnMut() -> T, T>(nb_it: usize, what: &'static str, mut f: F) -> T {
    let start = SystemTime::now();
    let mut result = f(); // iter 0
    for _ in 1..nb_it {
        result = f();
    }
    let end = SystemTime::now();
    let duration = end.duration_since(start).unwrap();
    println!("{nb_it} {} took {} ms", what, duration.as_millis());
    result
}
    

