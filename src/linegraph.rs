use crate::graph::*;

pub type Arc = Node; // index of a directed edge

pub struct LGraph {
    deg_sum: Vec<usize>,
    all_head: Vec<Node>,
    all_arc: Vec<Arc>, // identity
    rev_arc: Vec<Arc>,
}

impl AGraph for LGraph {

    fn n(&self) -> usize {
        * self.deg_sum.last().unwrap()
    }

    fn m(&self) -> usize {
        let mut m: usize = 0;
        for a in 0..self.n() {
            m += self.deg(a as Arc);
        }
        m
    }

    fn deg(&self, a: Arc) -> usize {
        let v = self.all_head[a as usize];
        let b = self.deg_sum[v as usize];
        let e = self.deg_sum[(v+1) as usize];
        e - b
    }

    fn neighbors(&self, a: Arc) -> &[Node] {
        let v = self.all_head[a as usize];
        let b = self.deg_sum[v as usize];
        let e = self.deg_sum[(v+1) as usize];
        &self.all_arc[b..e]
    }

}

impl LGraph {

    pub fn n_graph(&self) -> usize {
        self.deg_sum.len() - 1
    }

    pub fn rev_arc(&self, a: Arc) -> Arc {
        self.rev_arc[a as usize]
    }

    pub fn head(&self, a: Arc) -> Node {
        self.all_head[a as usize]
    }

    pub fn tail(&self, a: Arc) -> Node {
        self.head(self.rev_arc(a))
    }

    pub fn out_arcs(&self, v: Node) -> &[Node] {
        let b = self.deg_sum[v as usize];
        let e = self.deg_sum[(v+1) as usize];
        &self.all_arc[b..e]
    }

    pub fn in_arcs(&self, v: Node) -> &[Node] {
        let b = self.deg_sum[v as usize];
        let e = self.deg_sum[(v+1) as usize];
        &self.rev_arc[b..e]
    }


    // Undirected edges as input.
    pub fn from_edges(edg: &Vec<Edge>, self_loops: bool) -> Self {
        // Number of nodes:
        let mut n: Node = 0; // Max node number
        for e in edg {
            if e.u > n { n = e.u; }
            if e.v > n { n = e.v; }
        }
        let n = n + 1; // Number of nodes

        // Degrees:
        let mut deg_sum: Vec<usize> = vec![ if self_loops {1} else {0}; (n+1) as usize];
        deg_sum[n as usize] = 0;
        for e in edg {
            deg_sum[e.u as usize] += 1;
            deg_sum[e.v as usize] += 1;
        }
        // prefix sums:
        for u in 0..n {
            deg_sum[(u+1) as usize] += deg_sum[u as usize]
        }
        let m = deg_sum[n as usize];

        // Adjacency lists:
        let mut all_head: Vec<Node>  = vec![0; m];
        let mut all_arc: Vec<Arc>  = vec![0; m];
        let mut rev_arc: Vec<Arc>  = vec![0; m];
        let mut add_adj = |u: Node, v: Node| -> Arc {
            deg_sum[u as usize] -= 1;
            let a = deg_sum[u as usize];
            all_head[a] = v;
            all_arc[a] = a as Arc;
            a as Arc
        };
        for e in edg.iter().rev() {
            let uv = add_adj(e.u, e.v);
            let vu = add_adj(e.v, e.u);
            rev_arc[uv as usize] = vu;
            rev_arc[vu as usize] = uv;
        }

        // Self loops:
        if self_loops {
            for u in 0..n {
                let a = add_adj(u, u);
                rev_arc[a as usize] = a;
            }
        }

        Self { deg_sum, all_head, all_arc, rev_arc }
    }
}


#[cfg(test)]
pub mod tests {
    use super::*;

    use std::iter::zip;

    impl LGraph {
        pub fn check_graph(&self, g: &Graph) {
            for a in 0..self.n() as Arc {
                let v = self.head(a);
                    for (&b, &w) in zip(self.neighbors(a), g.neighbors(v)) {
                        assert_eq!(v, self.tail(b));
                        assert_eq!(w, self.head(b));
                    }
                }
        }
    }

    #[test]
    fn check_split() {
    }
}