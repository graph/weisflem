#[derive(Copy, Clone)]
pub struct Node(u32);

impl Into<usize> for Node {
    fn into(self) -> usize { self.0 as usize }
}

impl std::fmt::Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl Iterator for  Node {
    type Item = Node;
    fn next(&mut self) -> Option<Self::Item> {
        let v = *self;
        self.0 += 1;
        Some(v) 
    }
}

impl Node {
    pub fn all() -> Node { Node(0) }
    pub fn idx(&self) -> usize { self.0 as usize }
}

#[test]
fn test () {
    for v in Node::all().take(10) { println!("{v}") }
    let mut t = vec![0; 10];
    let _v = Node(1);
    for v in Node::all().take(10) { t[v.idx()] = v.into(); }
}