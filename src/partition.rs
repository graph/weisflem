use crate::{dygraph::Dygraph, graph::*};
pub type Elt = Node;
pub type Prt = Node; // index of a part 

// A part as an interval of the vector of elements.
pub struct Part {
    pub left: usize,
    pub right: usize,
    split: usize, // when splitting a part, the frontier of leaving elements, > right initially
    pub active: bool, // is the part active (pivot)
}

pub struct Partition {
    pub elements: Vec<Elt>,
    pub parts: Vec<Part>,
    pub pos_of: Vec<usize>, // position of an element in [elements]
    nb_parts: usize, // total number of parts
    pub part_of: Vec<Prt>, // part containing an element
    // for partition refinement:
    touched_parts: Vec<Prt>, // parts considered during a refinement step
    split_parts: Vec<Prt>, // parts effectively split into two during a refinement step
    multiplicity: Vec<Elt>, // count multiplicity of an element 
    set: Vec<Elt>, // unique elements
    count_by_mult: Vec<Elt>, // number of elements with given multiplicity
    //set_by_mult: Vec<Elt>, // unique elements sorted by multiplicity
    pub active_parts: Vec<Prt>, // newly created parts that can lead to further refinement
    pub unactivated_parts: Vec<Prt>, // parts that were not added to active_parts
}

// pub struct EltInfo {
//     part: Prt,
//     mult: Elt, // multiplicity
//     pos: usize,
// }

impl Part {
    pub fn len(&self) -> usize {
        return self.right - self.left;
    }
}

pub enum SplitOption { Left, Right, NoSplit }
use SplitOption::*;

use core::slice::Iter;

impl Partition {

    pub fn new(n: usize, active: bool) -> Self {
        let mut elements = Vec::with_capacity(n);
        let mut pos_of = Vec::with_capacity(n);
        for e in 0..n as Elt {
            elements.push(e);
            pos_of.push(e as usize);
        }
        let parts = Vec::with_capacity(n);
        let part_of = vec![0; n]; // 0 will be the first part containing all elements.
        let touched_parts = Vec::with_capacity(n);
        let split_parts = Vec::with_capacity(n);
        let multiplicity = vec![0; n];
        let set = Vec::with_capacity(n);
        let count_by_mult = vec![0; n+1];
        //let set_by_mult = Vec::with_capacity(n);
        let active_parts = Vec::with_capacity(n);
        let unactivated_parts = Vec::with_capacity(n);
        let mut ptt = Partition { elements, pos_of, parts, nb_parts: 1, part_of, 
            touched_parts, split_parts, multiplicity, set, count_by_mult, active_parts, unactivated_parts };
        ptt.parts.push(Part { left: 0, right:n, split: n+1, active });
        if active { ptt.active_parts.push(0); }
        ptt
    }

    pub fn len(&self) -> usize {
        assert_eq!(self.nb_parts, self.parts.len());
        self.nb_parts
    }

    pub fn clear(&mut self) {
        self.parts.clear();
        let n = self.elements.len();
        self.parts.push(Part { left: 0, right:n, split: n+1, active: true });
        self.nb_parts = 1;
        for e in 0..n {
            self.part_of[e] = 0;
        }
        self.active_parts.clear();
        self.unactivated_parts.clear();
    }

    pub fn iter(&self) -> impl IntoIterator<Item = &Part, IntoIter = Iter<'_, Part>> {
        self.parts.iter()
    }

    /// Swap elements in positions [i] and [j].
    pub fn swap(&mut self, i: usize, j: usize) {
        if i != j {
            self.elements.swap(i, j);
            self.pos_of[self.elements[i] as usize] = i;
            self.pos_of[self.elements[j] as usize] = j;
        }
    }

    // Split each part [P] in [P \cap set] and [P \setminus set].
    // multiplicity in [set] is ignored (each element is considered once).
    pub fn split_left(&mut self, set: &[Elt]) {
        for &e in set {
            let p = self.part_of[e as usize];
            let pt = &mut self.parts[p as usize];
            if pt.len() > 1 { // a singleton cannot be split!
                if pt.split > pt.right { // first time we see p
                    pt.split = pt.left;
                    self.touched_parts.push(p);
                }
                let pos = self.pos_of[e as usize];
                if pos >= pt.split { // first time we see e
                    let left = pt.split;
                    pt.split += 1;
                    self.swap(left, pos);
                }
            }
        }
        for &p in &self.touched_parts {
            let pt = &mut self.parts[p as usize];
            if pt.split < pt.right { // effective split of the part into two sub-parts
                self.nb_parts += 1; // count the child
                assert!(self.nb_parts <= self.elements.len());
                self.split_parts.push(p);
            } else {
                pt.split = pt.right+1; // reset
            }
        }
        self.touched_parts.clear();
        self.split_parts.sort_unstable(); // Canonical numbering: any permutation of the same set leads to the same numbering
        // Note: when adding k parts, this sort costs k log k <= k log n; and the total cost of sorting will be <= n log n. 
        for &p in &self.split_parts {
            let parent = &self.parts[p as usize];
            let p_len = parent.right - parent.split;
            let c_len = parent.split - parent.left;
            let active = parent.active || c_len >= p_len; // iterate on smaller part to reach O(m log n) complexity
            let child = self.parts.len() as Prt;
            self.parts.push(Part { left: parent.left, right:parent.split, split: parent.split+1, active });
            if active { self.active_parts.push(child); }
            let parent = &mut self.parts[p as usize];
            for &e in &self.elements[parent.left..parent.split] {
                self.part_of[e as usize] = child;
            }
            parent.left = parent.split;
            parent.split = parent.right+1;
            if (! parent.active) && ! active {
                parent.active = true;
                self.active_parts.push(p);
            }
        }
        self.split_parts.clear();
    }

    // Split each part [P] either in [P \cap set] and [P \setminus set],
    // or in [P \setminus set] and [P \cap set], or [P] (not split), 
    // depending on the position of [P] ([split_how] takes the left bound of [P] and returns
    // how to plit it).
    // Multiplicity in [set] is ignored (each element is considered once).
    // Returns true if some part was split.
    pub fn split_by_pos<FOpt: Fn(usize) -> SplitOption>(&mut self, set: &[Elt], split_how: FOpt) -> bool {
        for &e in set {
            let pos = self.pos_of[e as usize];
            let p = self.part_of[e as usize];
            let pt = &mut self.parts[p as usize];
            match split_how(pt.left) {
                Left => {
                    if pt.split > pt.right { // first time we see p
                        pt.split = pt.left;
                        self.touched_parts.push(p);
                    }
                    if pos >= pt.split { // first time we see e
                        let left = pt.split; // leftmost unseen elements
                        pt.split += 1;
                        self.swap(left, pos);
                    }
                }
                Right => {
                    if pt.split > pt.right { // first time we see p
                        pt.split = pt.right;
                        self.touched_parts.push(p);
                    }
                    if pos < pt.split { // first time we see e
                        pt.split -= 1;
                        let right = pt.split; // rightmost unseen elements
                        self.swap(pos, right);
                    }
                }
                NoSplit => (),
            }
        }
        let mut did_split = false;
        for &p in &self.touched_parts {
            let pt = &mut self.parts[p as usize];
            if pt.left < pt.split && pt.split < pt.right { // effective split of the part into two sub-parts
                self.nb_parts += 1; // count the child
                assert!(self.nb_parts <= self.elements.len());
                self.split_parts.push(p);
                did_split = true;
            } else {
                pt.split = pt.right+1; // reset
            }
        }
        self.touched_parts.clear();
        for &p in &self.split_parts {
            let parent = &self.parts[p as usize];
            let l_len = parent.split - parent.left;
            let r_len = parent.right - parent.split;
            let child_left = l_len <= r_len; // child is smallest so that part_of updates below also cost O(set.len()) overall
            let child = self.parts.len() as Prt;
            let (left, right) = 
                if child_left { (parent.left, parent.split) } else { (parent.split, parent.right) }; 
            self.parts.push(Part { left, right, split: right+1, active: true }); 
            self.active_parts.push(child); // iterate on smaller part to reach O(m log n) complexity
            for &e in &self.elements[left..right] {
                self.part_of[e as usize] = child; // part_of updates
            }
            let parent = &mut self.parts[p as usize];
            if child_left {
                parent.left = parent.split;
            } else {
                parent.right = parent.split;
            }        
            parent.split = parent.right+1;
            if ! parent.active {
                self.unactivated_parts.push(p);
            }
        }
        self.split_parts.clear();
        did_split
    }

    // Split each part [P] in [P \cap set_k], ..., [P \cap set_1],  and [P \setminus set].
    // where [set_i] are elements of [set] appearing [i] times ([k] is the maximum multiplicity)
    pub fn split_left_multi(&mut self, oset: &[Elt]) {
        if oset.is_empty() { return; }
        let mut set: Vec<Elt> = Vec::with_capacity(oset.len());
        set.extend(oset);
        set.sort_unstable();
        let mut uniq: Vec<(Elt, Elt)> = Vec::with_capacity(set.len());
        let mut prev = set[0];
        let mut mult: Elt = 0;
        for e in set {
            if e != prev { uniq.push((mult, prev)); mult = 0; prev = e; }
            mult += 1;
        }
        uniq.push((mult, prev)); // last element
        uniq.sort_unstable();

        let set:Vec<Elt> = uniq.iter().map(|&couple| couple.1).collect();
        let mut i = 0;
        let len = uniq.len();
        while i < len {
            let (m, _) = uniq[i];
            let mut j = i+1;
            while j < len && uniq[j].0 == m { j += 1; }
            self.split_left(&set[i..j]);
            i = j;
        }
    }

    // Split each part [P] in [P \cap set_k], ..., [P \cap set_1],  and [P \setminus set].
    // where [set_i] are elements of [set] appearing [i] times ([k] is the maximum multiplicity)
    pub fn split_left_multi_linear_complexity(&mut self, set: &[Elt]) {
        // if [set] has size < n / log n, use sorting!, otherwise:
        for &e in set {
            if self.multiplicity[e as usize] == 0 { 
                self.set.push(e); 
            }
            self.multiplicity[e as usize] += 1;
        }
        let mut min = self.elements.len() as Elt; // n
        let mut max = 0 as Elt;
        for &e in &self.set {
            let m = self.multiplicity[e as usize];
            if m < min { min = m; }
            if m > max { max = m; }
            self.count_by_mult[m as usize] += 1;
        }
        assert!(min > 0);
        for m in (min..=max).rev() { // suffix sums
            let m = m as usize;
            self.count_by_mult[m-1] += self.count_by_mult[m];
        }
        let mut set_by_mult = vec![0; self.set.len()]; 
        //self.set_by_mult.resize(self.set.len(), 0);
        for &e in &self.set {
            let m = self.multiplicity[e as usize] as usize;
            self.count_by_mult[m] -= 1;
            let pos = self.count_by_mult[m];
            set_by_mult[pos as usize] = e;
        }

        for m in (min..=max).rev() { // suffix sums
            let m = m as usize;
            let b = self.count_by_mult[m] as usize;
            let e = self.count_by_mult[m-1] as usize;
            self.split_left(&set_by_mult[b..e]);
        }

        // reset everything:
        for &e in &self.set {
            self.multiplicity[e as usize] = 0;
        }
        for m in (min-1)..=max {
            self.count_by_mult[m as usize] = 0;
        }
        self.set.clear();
        //self.set_by_mult.clear();
    }

    pub fn refine_neighbors_multi<G: AGraph>(&mut self, g_rev: &G) {
        let mut set = Vec::with_capacity(g_rev.m());
        while let Some(p) = self.active_parts.pop() {
            let pt = &mut self.parts[p as usize];
            pt.active = false;
            for &u in &self.elements[pt.left..pt.right] {
                for &v in g_rev.neighbors(u) { // in-neighbors for a digraph
                    set.push(v);
                }
            }
            self.split_left_multi(&set);
            set.clear();
        }
    }

    pub fn part_graph<G: AGraph>(&mut self, g: &G) -> Graph {
        let mut edg: Vec<Edge>  = Vec::with_capacity(g.m());
        for p in 0..self.parts.len() {
            let pt = &self.parts[p];
            let u = self.elements[pt.left];
            for &v in g.neighbors(u) { // out-neighbors for a digraph
                edg.push(Edge { u: p as Node, v: self.part_of[v as usize] as Node });
            }
        }
        Graph::from_edges(&edg, false)
    }
    

    pub fn from_graph_degrees<G: AGraph>(g: &G) -> Partition {
        let n = g.n();
        let mut ptt = Self::new(n, false);
        ptt.parts[0].right = 0;
        // count degrees
        for v in 0..n {
            let d = g.deg(v as Node);
            if d >= ptt.parts.len() {
                ptt.parts.resize_with(d+1, || Part { left: 0, right:0, split: n+1, active: false })
            }
            ptt.parts[d].right += 1;
        }
        ptt.nb_parts = ptt.parts.len();
        //prefix sum
        for p in 1..ptt.parts.len() {
            let prev = &ptt.parts[p-1];
            ptt.parts[p].left = prev.left + prev.right;
        }
        for p in 0..ptt.parts.len() {
            ptt.parts[p].right = ptt.parts[p].left;
        }
        // fill parts
        let mut add = |v: Elt, p: usize| {
            let i = ptt.parts[p].right;
            ptt.elements[i] = v;
            ptt.pos_of[v as usize] = i;
            ptt.parts[p].right = i + 1;
            ptt.part_of[v as usize] = p as Prt;
        };
        for v in 0..n {
            let d = g.deg(v as Node);
            add(v as Node, d);
        }
        ptt
    }

    pub fn core_ordering<G: AGraph>(&mut self, g: &G) -> usize {
        let mut highest_core = 0;
        for i in 0..self.elements.len() {
            let u = self.elements[i] as Node;
            let p = self.part_of[u as usize];
            highest_core = p;
            for &v in g.neighbors(u) {
                let q = self.part_of[v as usize];
                if p < q { self.move_left(v); }
            }
        }
        eprintln!("Highest core: {highest_core}");
        highest_core as usize
    }

    pub fn core(&self, k: usize) -> &[Elt] {
        if k >= self.parts.len() {
            &[]
        } else {
            let l = self.parts[k].left;
            let n = self.elements.len();
            &self.elements[l..n]
        }
    }

    fn move_left(&mut self, e: Elt) {
        let j = self.pos_of[e as usize];
        let p = self.part_of[e as usize] as usize;
        let i = self.parts[p].left;
        assert!(p > 0);
        self.swap(i, j);
        self.parts[p-1].right += 1;
        if self.parts[p-1].right >= self.parts[p-1].split { self.parts[p-1].split = self.parts[p-1].right + 1;}
        self.part_of[e as usize] = p as Prt - 1;
        self.parts[p].left += 1;
    }

}

pub fn graph_core<G: AGraph>(g: &G, k: usize) -> Graph {
    let mut ptt = Partition::from_graph_degrees(g);
    ptt.core_ordering(g);
    let edg = g.subgraph_edges(ptt.core(k));
    Graph::from_edges(&edg, false)
}

pub fn graph_core_ordering<G: AGraph>(g: &G) -> Vec<Node> {
    let mut ptt = Partition::from_graph_degrees(g);
    ptt.core_ordering(g);
    ptt.elements
}


use std::fmt;

impl fmt::Display for Partition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut res = f.write_str("{ ");
        for (i, &e) in self.elements.iter().enumerate() {
            assert_eq!(i, self.pos_of[e as usize]);
            if i != 0 && i == self.parts[ self.part_of[e as usize] as usize ].left {
                res = res.and(f.write_str("| "));
            }
            res = res.and(write!(f, "{e} "));
        }
        res.and(f.write_str("}"))
    }
}

pub fn lexbfs<G: AGraph>(g: &G, src: Node) -> Vec<Node> {
    let mut ptt = Partition::new(g.n(), false);
    let mut singleton = vec![0; 1];
    for i in 0..g.n() {
        let v = if i == 0 { src } else { ptt.elements[i] as Node };
        singleton[0] = v;
        ptt.split_left(&singleton);
        ptt.split_left(g.neighbors(v));
    }
    ptt.elements
}

pub fn low_stabbing_ordering<G: AGraph>(g: &G) -> Vec<Node> {
    let mut ptt = Partition::new(g.n(), false);
    let perm = random_permutation(g.n());
    for &v in &perm {
        ptt.split_left(g.neighbors(v));
    }
    let mut bfs = BFS::new(g.n());
    bfs.bfs_all(g, &perm);
    eprintln!("Crossing/stabbing number : {} (random :{}, bfs: {})", 
        Dygraph::crossing_number(g, &ptt.elements),
        Dygraph::crossing_number(g, &perm), 
        Dygraph::crossing_number(g, &bfs.fifo));     
    ptt.elements
}

#[cfg(test)]
pub mod tests {

    use std::fs::File;
    use std::io::{BufRead, BufReader};
    
    use super::*;

    impl Partition {
        fn check(&self) {
            for (i, &e) in self.elements.iter().enumerate() {
                assert_eq!(self.pos_of[e as usize], i);
            }
            let mut sum = 0;
            for (p, &ref pt) in self.parts.iter().enumerate() {
                assert!(pt.left <= pt.right);
                assert!(pt.right < pt.split);
                sum += pt.right - pt.left;
                for i in pt.left..pt.right {
                    assert_eq!(self.part_of[self.elements[i] as usize] as usize, p);
                }
            }
            assert_eq!(sum, self.elements.len());
            assert_eq!(self.nb_parts, self.parts.len());
        }
    }    

    #[test]
    fn check_split_by_pos() {
        let mut ptt = Partition::new(16, false);
        println!("{ptt}");
        ptt.split_by_pos(&[1, 2, 3, 4, 5, 7], |_| SplitOption::Left);
        println!("{ptt}"); // 1 2 3 4 5 7 | 0 6 8 9 ... 15
        assert_eq!(ptt.elements[0], 1);
        ptt.split_by_pos(&[12, 2, 10, 4,   12, 2, 10, 4], |_| SplitOption::Right);
        println!("{ptt}"); // 1 3 5 7 | 4 2 | 0 6 8 9 11 13 14 15 | 10 12
        let p = ptt.part_of[2] as usize;
        assert_eq!(ptt.parts[p].left, 4);
        assert_eq!(ptt.parts[p].right, 6);
        assert_eq!(ptt.elements[15], 12);
        ptt.split_by_pos(&[14, 7, 15, 4,   14, 7, 15, 4, 14, 7, 15, 4, 14, 7, 15, 4], |pos| 
            if pos == 4 {SplitOption::NoSplit} else if pos < 4 {SplitOption::Left} else {SplitOption::Right}
        );
        println!("{ptt}"); // 7 | 1 3 5 | 4 2 | 0 6 8 9 11 13 | 15 14 | 10 12
        let p = ptt.part_of[ptt.elements[4] as usize] as usize;
        assert_eq!(ptt.parts[p].left, 4);
        assert_eq!(ptt.parts[p].right, 6);
        assert_eq!(ptt.elements[0], 7);
        assert!(ptt.parts[ptt.part_of[13] as usize].left < ptt.parts[ptt.part_of[14] as usize].left);
        ptt.check();
        assert_eq!(ptt.len(), 6);
    }

    #[test]
    fn check_split_multi() {
        let mut ptt = Partition::new(16, false);
        println!("{ptt}");
        ptt.split_left_multi(&[1, 2, 3, 4, 5, 7, 3]);
        println!("{ptt}");
        assert_eq!(ptt.parts[ptt.part_of[3] as usize].len(), 1);
        ptt.split_left_multi(&[12, 2, 10, 4]);
        println!("{ptt}");
        let p = ptt.part_of[2] as usize;
        assert_eq!(ptt.parts[p].len(), 2);
        ptt.split_left_multi(&[14, 7, 15, 14, 14, 4]);
        println!("{ptt}");
        assert!(ptt.parts[ptt.part_of[4] as usize].left < ptt.parts[ptt.part_of[2] as usize].left);
        ptt.split_left_multi(&[4, 7, 12, 14, 14, 8, 9, 9, 8]);
        println!("{ptt}");
        ptt.check();
        assert_eq!(ptt.len(), 11);
    }

    #[test]
    fn check_core() {
        let file = File::open("test/hex2.txt")
        .expect("Should have been able to read the file");
        let edg = parse_edges(BufReader::new(file).lines());
        let g = Graph::from_edges(&edg, true);
        let mut ptt = Partition::from_graph_degrees(&g);
        println!("{ptt}");
        ptt.check();
        let dege = ptt.core_ordering(&g);
        eprintln!("Degeneracy: {}", dege);
        ptt.check();
    }    
}