
use crate::graph::*;
use crate::dygraph::*;
use crate::partition::*;

/// Compute a factorizing permutation where any strong module is a factor.
/// Based on https://doi.org/10.1142/S0129054199000125 [Habib, Paul, Viennot, 1999]
pub fn factor_perm_orig<G: AGraph>(g: &G) -> Vec<Node> {
    let mut ptt = Partition::new(g.n(), false);
    let mut non_singleton = Vec::with_capacity(g.n());
    let mut first_pivot: Vec<Option<Node>> = vec![None; g.n()];
    let mut last_pushed: Prt = 0;
    let mut push_new_parts = |queue: &mut Vec<Prt>, len: usize| {
        while (last_pushed as usize) < len {
            queue.push(last_pushed);
            last_pushed += 1;
        }
    };
    push_new_parts(&mut non_singleton, ptt.len());

    while let Some(p) = non_singleton.pop() {

        // Take a non_singleton part [p] (it must be a module):
        let pt = & ptt.parts[p as usize];
        assert!( ! pt.active);
        if pt.len() <= 1 { continue } // singleton indeed
        //assert!(is_module(g, &ptt.elements[pt.left..pt.right])); // fails with as-skitter

        // Take an element [x] in [p]:
        let x = match first_pivot[p as usize] {
            None => ptt.elements[pt.left],
            Some(x) => x,
        };
        assert!(ptt.part_of[x as usize] == p);

        // Split [p] into [p\cap \bar N(x), {x}, p\cap N(x)]:
        assert!( ! g.has_neighbor(x, x)); // TODO: allow loops
        ptt.split_by_pos(g.neighbors(x), |_| SplitOption::Right);
        ptt.split_by_pos(&[x], |_| SplitOption::Right);
        push_new_parts(&mut non_singleton, ptt.len());
        first_pivot[p as usize] = None;
        non_singleton.push(p); // p is now one of p\cap \bar N(x), {x}, p\cap N(x)

        // Split direction when splitting a part [pos,_) by a pivot in [pivot,_): 
        let center = ptt.pos_of[x as usize];
        assert_eq!(center, ptt.parts[ptt.part_of[x as usize] as usize].left);
        let split_how = |pivot, pos| -> SplitOption {
            if pos == pivot || pos == center { return SplitOption::NoSplit }
            if (pivot < pos && pos < center)|| (center < pos && pos < pivot) {
                SplitOption::Left
            } else {
                SplitOption::Right
            }
        };
        //println!("{ptt}");

        // Partition refinement:
        while ! (ptt.active_parts.is_empty() && ptt.unactivated_parts.is_empty()) {
            while let Some(pivot) = ptt.active_parts.pop() {
                let pt = &mut ptt.parts[pivot as usize];
                pt.active = false;
                let (left, right) = (pt.left, pt.right);
                // refine others:
                for i in left .. right {
                    let v = ptt.elements[i];
                    ptt.split_by_pos(g.neighbors(v), |pos| split_how(left, pos));
                    //println!("{v} {ptt}");
                }
                let pt = & ptt.parts[pivot as usize];
                assert_eq!((left, right), (pt.left, pt.right));
            }
            while let Some(module) = ptt.unactivated_parts.pop() {
                match first_pivot[module as usize] {
                    Some(x) if ptt.part_of[x as usize] == module => {
                        // useless to pivot on x again
                    }
                    _ => {
                        let pt = & ptt.parts[module as usize];
                        // a priori a module (with respect to all used pivots), but not necessarily
                        assert!( ! pt.active);
                        let x = ptt.elements[pt.left];
                        first_pivot[module as usize] = Some(x);
                        let left = pt.left;
                        if ptt.split_by_pos(g.neighbors(x), |pos| split_how(left, pos)) { 
                            break 
                        }
                    }
                }
            }
        }

        push_new_parts(&mut non_singleton, ptt.len());
    }

    ptt.elements
}

/// Compute a factorizing permutation where any strong module is a factor.
/// Version where we refine the pivot part according to neighboring parts.
/// Simpler to implement, but seems slightly slower than factor_perm().
pub fn factor_perm_pivot<G: AGraph>(g: &G) -> Vec<Node> {
    let mut ptt = Partition::new(g.n(), false);
    let mut non_singleton = Vec::with_capacity(g.n());
    let mut last_pushed: Prt = 0;
    let mut push_new_parts = |queue: &mut Vec<Prt>, len: usize| {
        while (last_pushed as usize) < len {
            queue.push(last_pushed);
            last_pushed += 1;
        }
    };
    push_new_parts(&mut non_singleton, ptt.len());
    let mut pivot_graph  = Dygraph::new(g.n()); // for constructing graph pivots -> other parts

    while let Some(p) = non_singleton.pop() {

        // Take a non_singleton part [p] (it must be a module):
        let pt = & ptt.parts[p as usize];
        assert!( ! pt.active);
        if pt.len() <= 1 { continue } // singleton indeed

        // Take an element [x] in [p] and split [p] into [p\cap \bar N(x), {x}, p\cap N(x)]:
        let x = ptt.elements[pt.left];
        assert!( ! g.has_neighbor(x, x)); // TODO: allow loops
        ptt.split_by_pos(g.neighbors(x), |_| SplitOption::Right);
        ptt.split_by_pos(&[x], |_| SplitOption::Right);
        push_new_parts(&mut non_singleton, ptt.len());
        non_singleton.push(p); // p is now one of p\cap \bar N(x), {x}, p\cap N(x)    

        // Split direction when splitting a part [pos,_) by a pivot in [pivot,_): 
        let center = ptt.pos_of[x as usize];
        let split_how = |pivot, pos| -> SplitOption {
            if pos == pivot || pos == center { return SplitOption::NoSplit }
            if (pivot < pos && pos < center)|| (center < pos && pos < pivot) {
                SplitOption::Left
            } else {
                SplitOption::Right
            }
        };
        //println!("{ptt}");
        
        // Partition refinement:
        while let Some(pivot) = ptt.active_parts.pop() {
            let pt = &mut ptt.parts[pivot as usize];
            pt.active = false;
            let (left, right) = (pt.left, pt.right);
            // refine others:
            for i in left .. right {
                let v = ptt.elements[i];
                ptt.split_by_pos(g.neighbors(v), |pos| split_how(left, pos));
                //println!("{v} {ptt}");
            }
            // refine [pivot]:
            pivot_graph.clear();
            for i in left .. right {
                let v = ptt.elements[i];
                for &w in g.neighbors(v) {
                    let pw = ptt.part_of[w as usize];
                    if pw != pivot { 
                        pivot_graph.add_arc(pw as Node, v);
                    }
                }
            }
            //Not necessary and not faster: pivot_graph.simple();
            for &q in pivot_graph.sources() {
                let qt = & ptt.parts[q as usize];
                let pivot = qt.left;
                assert!(pivot != left);
                ptt.split_by_pos(pivot_graph.neighbors(q), |_| split_how(pivot, left));
                //println!("{q} {q_elt} {ptt}");
            }
        }

        push_new_parts(&mut non_singleton, ptt.len());
    }

    ptt.elements
}


pub fn factor_perm<G: AGraph>(g: &G) -> Vec<Node> { factor_perm_orig(g) } // a bit faster


pub struct TreeDec {
    pub perm: Vec<Node>, // a factorizing permutation (leaves of the tree in DFS order)
    pub smodules: Vec<(Node, Node)>, // strong modules as intervals of [perm]
    pub parent: Vec<Node>, // parent[i] is the index of the parent of smodules[i] in smodules
    pub tree: Dygraph, // tree.neighbors(i) lists the children of smodules[i]
}

enum IntervalBound {
    Left(Node),
    Right(Node),
}
impl IntervalBound {
    fn bound(&self) -> Node {
        match *self {
            Left(k) => k,
            Right(k) => k,
        }
    }
}
use IntervalBound::*;

impl TreeDec {

    /// Compute a tree decomposition from a factorizing permutation [perm].
    /// Based on https://doi.org/10.1137/06065133 [Bergeron, Chauve, De Montgolfier, Raffinot, 2008]
    /// Returns basically a triple [(smodules, parent, tree)] 
    /// where [smodules] lists intervals of [perm] that form strong modules 
    ///   ([smodules[i]=(l,r)] if [i] represents the strong module perm[l..=r]),
    /// [tree] gives the tree structure between them: tree.neighbors(i) lists the children of mod[i],
    ///  and [parent] gives the parent of a module (parent[j]=i for all i in tree.neoighbors(i)).
    /// The root is 0 ([mod[0]=0..ord.len()] spans completely [ord]). 
    pub fn from_factor_perm<G: AGraph>(g: &G, mut perm: Vec<Node>) -> TreeDec {
        let n = g.n();
        let mut g = Dygraph::from_agraph(g);
        g.sort();

        assert_eq!(n, perm.len());

        // Compute a representation of modules as two arrays [left, right] of indexes such that
        // for all i left[i] <= i and right[i] >= i,
        // perm[i..=j] is a module iff right[i] >= left[j].
        fn right_modules (g: &Dygraph, perm: &[Node]) -> Vec<Node> {
            let n = perm.len();
            let perm = Permutation::new(perm.to_vec());
            let inv = perm.inverse();
            let mut rightmost_splitter: Vec<Node> = (0..n as Node).collect();
            for i in 1..n {
                let u = perm.ord[i-1];
                let v = perm.ord[i];
                g.adj_diff_iter(u, v, |x| {
                    let ix = inv.ord[x as usize];
                    if ix > rightmost_splitter[i] {
                        rightmost_splitter[i] = ix;
                    } 
                });
            }
            let mut left: Vec<Node> = (0..n as Node).collect();
            let mut stack = Vec::new();
            for i in (1..n).rev() {
                stack.push(i as Node);
                if rightmost_splitter[i] as usize > i {
                    while let Some(t) = stack.pop() {
                        if t < rightmost_splitter[i] {
                            left[t as usize] = i as Node;
                        } else {
                            stack.push(t); // unpop
                            break
                        }
                    }
                }
            }
            while let Some(t) = stack.pop() {
                left[t as usize] = 0;
            }
            left
        }

        let mut left = right_modules(&g, &perm);
        perm.reverse();
        let mut right = right_modules(&g, &perm);
        fn index_reverse(right: &mut Vec<Node>) {
            let n = right.len() as Node;
            right.reverse();
            for r in right.iter_mut() {
                *r = (n - 1) - *r; 
            }
        }
        index_reverse(&mut right);
        perm.reverse();
        //eprintln!("left: {:?}", left);
        //eprintln!("right: {:?}", right);

        // Compute a canonical representation of the family of intervals represented by [left, right].
        fn canonical_right(left: &[Node], right: &[Node]) -> Vec<Node> {
            assert_eq!(left.len(), right.len());
            let n = left.len();

            // computation of support with respect to [right]:
            let mut support: Vec<Node> = (0..n as Node).collect();
            let mut stack: Vec<Node> = Vec::new();
            stack.push(0);
            for i in 1..n {
                while let Some(t) = stack.pop() {
                    if right[t as usize] as usize >= i {
                        support[i] = t;
                        stack.push(t); // unpop
                        break
                    }
                }
                stack.push(i as Node);
            }
            //eprintln!("support: {:?}", support);

            // computation of canonical right:
            let mut can_right: Vec<Node> = (0..n as Node).collect();
            can_right[0] = n as Node - 1;
            for k in (1..n).rev() {
                let l = support[k] as usize; // what if support[k] = k ? (undefined)
                //   if (k..=right[k]) is in the family and non-singleton, then support[k+1] = k
                //   since right is commuting (i.e (k..=righ[k]) cannot overlap (k'..=right[k']) for any k,k')
                // so do nothing if support[k] = k 
                if l == k { continue }
                assert!(l < k);
                let r = can_right[k] as usize;
                assert!(k <= r);
                // is (l..=r) in the family represented by left, right:
                let in_family = right[l] as usize >= r && left[r] as usize <= l; 
                if in_family {
                    can_right[l] = std::cmp::max(r as Node, can_right[l])
                }
            }

            can_right
        }

        let can_right = canonical_right(&left, &right);
        index_reverse(&mut left);
        index_reverse(&mut right);
        let mut can_left = canonical_right(&right, &left);
        index_reverse(&mut can_left);
        let left = can_left;
        let right = can_right;
        //eprintln!("left: {:?}", left);
        //eprintln!("right: {:?}", right);

        // Compute strong intervals:
        let mut bounds = Vec::with_capacity(4*n);
        for (i, &li) in left.iter().enumerate() {
            bounds.push(Left(li));
            bounds.push(Right(i as Node));
        }
        for (i, &ri) in right.iter().enumerate() {
            bounds.push(Left(i as Node));
            bounds.push(Right(ri));
        }
        bounds.sort_unstable_by(|b, c| 
            match b.bound().cmp(&c.bound()) {
                std::cmp::Ordering::Equal => 
                    match (b, c) { // left bounds first
                        (Left(_), Right(_)) => std::cmp::Ordering::Less,
                        (Right(_), Left(_)) => std::cmp::Ordering::Greater,
                        _ => std::cmp::Ordering::Equal,
                    },
                c => c,
            }
        );
        //
        let mut strong_intervals: Vec<(Node,Node)> = Vec::with_capacity(2*n); // at least n, at most 2n
        let mut stack: Vec<Node> = Vec::new();
        for b in bounds {
            match b {
                Left(l) => stack.push(l),
                Right(r) => {
                    let l = stack.pop().unwrap();
                    strong_intervals.push((l,r));
                }
            }
        }
        strong_intervals.sort_unstable_by(|&(l1,r1), &(l2,r2)| -> std::cmp::Ordering {
            match l1.cmp(&l2) {
                std::cmp::Ordering::Equal => r1.cmp(&r2).reverse(),
                c => c
            }
        });
        strong_intervals.dedup();
        strong_intervals.shrink_to_fit();
        assert!(strong_intervals.len() > 0 && strong_intervals[0] == (0, n as Node - 1));
        //
        let m = strong_intervals.len();
        let mut ipar: usize = 0; // first parent is strong_intervals[0]
        let mut parent: Vec<Node> = (0..m as Node).collect();
        let mut k = 1;
        while k < m {
            assert!(ipar < k);
            let sik = &strong_intervals[k];
            let par = &strong_intervals[ipar];
            let included_in_par = par.0 <= sik.0 && sik.1 <= par.1;
            if included_in_par {
                parent[k] = ipar as Node;
                ipar = k;
                k += 1;
            } else {
                ipar = parent[ipar] as usize;  
            }
        }

        let mut tree = Dygraph::new(strong_intervals.len());
        for (i, &p) in parent.iter().enumerate() {
            if p as usize != i { tree.add_arc(p as Node, i as Node) }
        }
        tree.sort();

        TreeDec { perm, smodules: strong_intervals, parent, tree }
    }

    pub fn left_node(&self, i: Node) -> Node {
        self.perm[self.smodules[i as usize].0 as usize]
    }

    pub fn quotient<G: AGraph>(&self, g: &G, i: Node) -> LGraph {
        if self.tree.deg(i) == 0 { return LGraph::node(self.left_node(i)) }
        let vtx: Vec<Node> = self.tree.neighbors(i).iter().map(|&j| 
            self.left_node(j) // a representant of [j]th child
        ).collect();
        let edg = g.subgraph_edges(&vtx);
        let edg = edg.into_iter().filter(|e| e.u < e.v).collect();
        LGraph::from_edges(edg, vtx.len())
    }
}


pub fn is_module<G: AGraph>(g: &G, set: &[Node]) -> bool {
    let minmod = min_module(g, set);
    if minmod.len() != set.len() {
        eprintln!("min_module({:?}) = {:?}", set, minmod)
    }
    minmod.len() == set.len()
}


pub fn min_module<G: AGraph>(g: &G, set: &[Node]) -> Vec<Node> {
    let mut ptt = Partition::new(g.n()+2, false); // add two dummy node
    let v_univ = g.n() as Node; // neighbor of everybody
    let v_isol = v_univ + 1; // non neighbor of every body
    let mut module = Vec::with_capacity(set.len());
    module.extend(set);
    // isolate nodes of [set]
    for &v in set {
        ptt.split_left(&[v]);
    }
    // nodes to refine on:
    let mut from = 0; // index in module
    let mut prev_l = 0;
    let mut prev_r = 0;
    while from < module.len() {
        // refine:
        for i in from..module.len() {
            let v = module[i];
            let mut neigh: Vec<Node> = vec![v_univ; 1];
            neigh.extend(g.neighbors(v)); // dummy neighbor
            ptt.split_left(&neigh);
        }
        from = module.len();
        // non splitters:
        let fst = ptt.part_of[v_univ as usize];
        let lst = ptt.part_of[v_isol as usize];
        // splitters:
        let l = ptt.parts[fst as usize].right;
        let r = ptt.parts[lst as usize].left;
        if prev_l >= prev_r { prev_l = r; prev_r = r; }
        for i in (l..prev_l).chain(prev_r..r) {
            let splitter = ptt.elements[i];
            module.push(splitter);
            ptt.split_left(&[splitter]); // should not move (just split its part)
            assert!(ptt.pos_of[splitter as usize] == i);
        }
        prev_l = l; prev_r = r;
    }
    module.sort_unstable();
    module
}

/// An undirected graph with labeled vertices.
#[derive(Eq)]
pub struct LGraph {
    edg : Vec<Edge>, // only edge [uv] with [u < v] is stored (no loops)
    vtx : Vec<Node>, // all nodes appearing in edges, plus isolated nodes
}

impl PartialEq for LGraph {
    fn eq(&self, other: &Self) -> bool {
        self.vtx == other.vtx && self.edg == other.edg
    }
}

impl LGraph {

    pub fn new(edg: &[(Node, Node)], isolated: &[Node]) -> LGraph {
        let mut vtx = isolated.to_vec();
        let mut edg: Vec<Edge> = edg.iter()
            .map(|&(u,v)| -> Edge { 
                assert!(u < v); 
                vtx.push(u);
                vtx.push(v);
                Edge { u, v } 
            }).collect();
        edg.sort_unstable();
        vtx.sort_unstable();
        vtx.dedup();
        vtx.shrink_to_fit();
        LGraph { edg, vtx }
    }

    pub fn from_edges(edg: Vec<Edge>, n: usize) -> LGraph {
        let vtx: Vec<Node> = (0..n as Node).collect();
        for &e in &edg {
            assert!(e.u < n as Node);
            assert!(e.v < n as Node);
        }
        LGraph { edg, vtx }
    }

    pub fn node(v: Node) -> LGraph {
        LGraph { edg: Vec::with_capacity(0), vtx: vec![v; 1] }
    }

    pub fn n(&self) -> usize {
        self.vtx.len()
    }

    pub fn m(&self) -> usize {
        self.edg.len()
    }

    pub fn compose(&self, sons: &[LGraph]) -> LGraph {
        assert!(self.labels_in_zero_n());
        assert_eq!(self.n(), sons.len());

        // composition size:
        let mut n = 0 as usize;
        let mut m = 0 as usize;
        for h in sons { n += h.n(); m += h.m() }
        for &e in &self.edg { m += sons[e.u as usize].n() * sons[e.v as usize].n() }

        // nodes:
        let mut vtx = Vec::with_capacity(n);
        for h in sons {
            for &v in &h.vtx {
                vtx.push(v);
            }
        }
        vtx.sort_unstable();
        vtx.dedup(); // check that there are no duplicates:
        assert_eq!(vtx.len(), n);

        // edges:
        let mut edg = Vec::with_capacity(m);
        for h in sons { edg.extend(&h.edg) }
        for &e in &self.edg { 
            for &u in &sons[e.u as usize].vtx {
                for &v in &sons[e.v as usize].vtx {
                    let uv = if u < v { Edge { u, v } } else { Edge { u: v, v: u } };
                    edg.push(uv);
                }
            }
        }
        edg.sort_unstable();

        LGraph { edg, vtx }
    }

    pub fn labels_in_zero_n(&self) -> bool {
        let n = self.n() as Node;
        for &e in &self.edg {
            if e.u >= n ||  e.v >= n { return false }
        }
        for &v in &self.vtx {
            if v >= n { return false }
        }
        true
    }

}

use std::fmt::Display;

impl Display for LGraph {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;
        let mut fst = true;
        for &e in &self.edg {
            if fst { fst = false } else { write!(f, ", ")?; }
            write!(f, "({},{})", e.u, e.v)?;
        }
        write!(f, "]   (")?;
        let mut fst = true;
        for &v in &self.vtx {
            if fst { fst = false } else { write!(f, ", ")?; }
            write!(f, "{v}")?;
        }
        write!(f, ")")?;
        Ok(())
    }
}

pub struct Permutation { ord: Vec<Node> }

impl Permutation {

    pub fn new(ord: Vec<Node>) -> Permutation {
        let p = Permutation { ord };
        p.inverse(); // check that we have a permutation
        p
    }

    pub fn inverse(&self) -> Permutation {
        let n = self.ord.len() as Node;
        let init = n; // a value not in 0..n
        let mut inv = vec![init; self.ord.len()];
        for (i, &v) in self.ord.iter().enumerate() {
            assert!(v < n);
            assert_eq!(inv[v as usize], init);
            inv[v as usize] = i as Node;
        }
        Permutation { ord: inv }
    }

    pub fn is_factor(&self, set: &[Node]) -> bool {
        self.inside_factor(set).len() == 0
    }

    pub fn at_most_two_factors(&self, set: &[Node]) -> bool {
        let inside = self.inside_factor(set);
        inside.len() == 0 || self.is_factor(&inside)
    }

    pub fn inside_factor(&self, set: &[Node]) -> Vec<Node> {
        let inv = self.inverse();
        let mut seen = vec![false; self.ord.len()];
        let mut min = self.ord.len();
        let mut max = 0 as usize;
        for &v in set {
            let i = inv.ord[v as usize] as usize;
            seen[i] = true;
            if i < min { min = i }
            if i > max { max = i }
        }
        let mut inside = Vec::new();
        for i in min..=max {
            if ! seen[i] { inside.push(self.ord[i]) }
        }
        inside
    }
    
}


pub fn check_two_modules<G: AGraph>(g: &G, ord:&[Node]) {
    assert_eq!(ord.len(), g.n());
    let mut perm = Vec::new();
    perm.extend(ord);
    let perm = Permutation::new(perm);
    for u in 0..g.n() as Node {
        for v in u+1 .. g.n() as Node {
            let m = min_module(g, &[u, v]);
            if ! perm.is_factor(&m) {
                eprintln!("not factor: {:?} = min_module({},{})", m, u, v);
                assert!(perm.at_most_two_factors(&m));
                let mut inter = perm.inside_factor(&m);
                assert!(is_module(g, &inter));
                inter.extend(&m);
                assert!(is_module(g, &inter));
            }
        }
    }
}

#[cfg(test)]
pub mod tests {

    use super::*;

    #[allow(dead_code)]
    pub const BOAT: [(Node, Node); 18] = [
        (0, 1), (0, 2),
        (1, 2), 
        (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8),
        (3, 4), 
        (4, 5), (4, 6), (4, 7),
        (5, 6), (5, 7),
        (6, 7), (6, 8),
        (7, 8)
    ];
    // (0 -- 1) -- 2 -- (3 -- 4 -- (6 -- 7) -- 8)
    //                          \ /
    //                           5

    #[test]
    fn check_factor_perm() {
        let edg = BOAT.map(|(u,v)| Edge { u, v });
        let g = Graph::from_edges(&edg, true);
        let perm = factor_perm(&g);
        println!("{:?}", perm);
        let perm = factor_perm_pivot(&g);
        println!("{:?}", perm);
        let perm = Permutation::new(perm);
        assert!(perm.is_factor(&[0, 1]));
        assert!(perm.is_factor(&[6, 7]));
        assert!(perm.is_factor(&[3, 4, 5, 6, 7, 8]));
        println!("{:?}", min_module(&g, &[6, 7]));
        assert!(min_module(&g, &[6, 7]) == &[6, 7]);
        println!("{:?}", min_module(&g, &[5, 7]));
        assert!(min_module(&g, &[5, 7]) == &[3, 4, 5, 6, 7, 8]);
        println!("{:?}", min_module(&g, &[0, 1]));
        assert!(min_module(&g, &[0, 1]) == &[0, 1]);
    }

    #[test]
    fn check_compose() {
        let g0 = LGraph::new(&[(0,1)], &[]);
        let g67 = LGraph::new(&[(6,7)], &[]);
        let g2 = LGraph::new(&[(0,1), (1,2), (2,3), (1,4), (2,4)], &[]);
        let v = |u: Node| LGraph::node(u);
        let g2 = g2.compose(&[v(3), v(4), g67, v(8), v(5)]);
        let g = LGraph::new(&[(0, 1), (1, 2)], &[]);
        let g = g.compose(&[g0, v(2), g2]);
        for &e in &g.edg {
            println!("({}, {})", e.u, e.v);
        }
        let boat = LGraph::new(&BOAT, &[]);
        assert!(g == boat);
    }

    #[test]
    fn check_two_modules_ex() {
        let n = |u: Node| LGraph::node(u);
        let par = |u, v| LGraph::new(&[], &[u, v]);
        let par3 = |u, v, w| LGraph::new(&[], &[u, v, w]);
        let ser = |u, v| LGraph::new(&[(u, v)], &[]);
        let p4 = LGraph::new(&[(0,1), (1,2), (2,3)], &[]);
        let (a,b,c,d,e,f,g) = (0,1,2,3,4,5,6);
        let (u,v,w,x,y,z) = (7,8,9,10,11,12);
        let fig3up = p4.compose(&[par(v, w), n(u), ser(c, d), par3(x, y, z)]);
        let fig3dw = p4.compose(&[par(b, e), n(a), n(f), n(g)]);
        let p2 = LGraph::new(&[(0,1)], &[]);
        let fig3 = p2.compose(&[fig3up, fig3dw]);
        for &e in &fig3.edg {
            println!("{} {}", e.u, e.v)
        }
        let fig3 = Graph::from_edges(&fig3.edg, true);
        println!("Graph with n={} nodes and m={} edges.", fig3.n(), fig3.m()/2);
        let perm = factor_perm(&fig3);
        println!("factor perm: {:?}", perm);
        // [6, 0, 4, 1, 5, 8, 9, 7, 11, 10, 12, 3, 2] = g a e b f v w u y x z d c
        check_two_modules(&fig3, &perm);
        let td = TreeDec::from_factor_perm(&fig3, perm);
        let (smodules, parent, tree) = (&td.smodules, &td.parent, &td.tree);
        for (i, &(l, r)) in smodules.iter().enumerate() {
            let (lp, rp) = smodules[parent[i] as usize];
            println!("strong module perm[{l}..={r}] = {:?} parent={lp}..={rp} deg={}", 
                     &td.perm[l as usize ..= r as usize], tree.deg(i as Node));
            println!("  quotient: {}", td.quotient(&fig3, i as Node));
            assert!(is_module(&fig3, &td.perm[l as usize ..= r as usize]));
        }
    }

}